@extends('admin.master')

@section('css')

@endsection
@section('js')
    <script>
        function xacthuc() {
            var r = confirm('Ban co muon xoa k');
            if (r) {
                location.href = $(this).attr('href');
            }
        }
        function getHtmlForUtility(utilities)
        {
            var html = "";
            var stt = 1;
            utilities.forEach(utility =>{
                html += `<tr><td>${stt++}</td><td>${utility.name}</td><td><a href="admin/utility/edit/${utility.id}" class="btn btn-primary">Sửa</a><a href="admin/utility/destroy/${utility.id}" onclick="return confirm('Bạn có muốn xóa!')" class="btn btn-danger">Xóa</a></td></tr>`;
            });
            $('tbody').html(html);
        }
        $('#searchUtilities').keyup(function () {
            $dataSearch = $('#formSearchUtility').serialize();
            urlResource = '{{route('admin.utility.search')}}';
            type = 'get';
            callApi($dataSearch, urlResource, type)
                .done(response => {
                    getHtmlForUtility(response.data);
                })
        })

    </script>
@endsection
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Tiện ích</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Tiện ích</a></li>
                    <li class="active"><a href="index.html">Danh sách</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="container">
                                <div class="row">
                                    @if(session('message'))
                                        <div class="alert bg-primary">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold">Well done!</span> {{session('message')}}
                                        </div>
                                    @endif

                                    @if(count($errors)>0)
                                        <div class="alert bg-danger">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold"><b>Lỗi!</b></span><br>
                                            @foreach($errors->all() as $err)
                                                {{$err}}<br>
                                            @endforeach
                                        </div>
                                    @endif

                                    <div class="card">
                                        <div class="input-group well w100">
                                            <form action="" id="formSearchUtility">
                                                <input type="text" placeholder="Search..." class="form-control"
                                                       name="search" id="searchUtilities">
                                            </form>
                                        </div>

                                        <div class="card-body">
                                            <table id="listUtilities" class="table table-bordered text-justify">
                                                <thead>
                                                <tr>
                                                    <th style="width: 12px;">STT</th>
                                                    <th>Tiện ích</th>
                                                    <th>Hành động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($utilities as $utility)
                                                    <tr>
                                                        <td>{{$stt++}}</td>
                                                        <td>{{$utility->name}}</td>
                                                        <td>
                                                            <a href="{{route('admin.utility.edit',['id'=>$utility->id])}}"
                                                               class="btn btn-primary">Sửa
                                                            </a>
                                                            <a href="{{route('admin.utility.destroy',['id'=>$utility->id])}}"
                                                               class="btn btn-danger"
                                                               data-id="{{$utility->id}}"
                                                               onclick="return confirm('Bạn có muốn xóa không?');">Xóa
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="pull-left">{{$utilities->links()}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection
