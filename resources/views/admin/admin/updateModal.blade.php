<!--modal add -->
<div class="modal fade" id="updateModalAdmin">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Admin</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="" id="formUpdateAdmin" enctype="multipart/form-data">
                    @csrf
                    <input type="text" class="hidden" name="id" id="idAdminEdit">
                    <div class="form-group">
                        <label for="name" class="col-xs-4 control-label">Tên</label>
                        <div class="col-xs-8">
                            <div class="nameError" style="color: red">

                            </div>
                            <input type="text" class="form-control" id="nameEdit" name="name" placeholder="Nhập tên">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-xs-4 control-label">Biệt danh</label>
                        <div class="col-xs-8">
                            <div class="usernameError" style="color: red">

                            </div>
                            <input type="text" class="form-control" id="usernameEdit" name="username"
                                   placeholder="Nhập biệt danh">

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-xs-4 control-label">Email</label>
                        <div class="col-xs-8">
                            <div class="emailError" style="color: red">

                            </div>
                            <input type="text" class="form-control" id="emailEdit" name="email" placeholder="Nhập Email">

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-xs-4 control-label">Avatar</label>
                        <button type="button" class="close" style="display: none"><span>×</span><span
                                class="sr-only">Close</span></button>
                        <div class="col-xs-8">
                            <div class="avatar" style="height: 200px">
                                <img src="" alt=""
                                     style="height: 190px" id="avatarImageEdit" name="avatarImage">
                            </div>
                            <input type="file" class="form-control hidden"
                                   name="avatar"
                                   id="avatarEdit">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="col-xs-4 control-label">Trạng thái</label>
                        <div class="col-xs-8">
                            <select name="status" id="statusEdit" class="form-control">
                                <option value="1" id="active">Hoạt động</option>
                                <option value="0" id="disActive">Tạm khóa</option>
                            </select>
                        </div>
                    </div><hr>

                    <div class="footer">
                        <button type="submit" class="btn btn-primary" id="btnUpdateResourceAdmin">Lưu</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
