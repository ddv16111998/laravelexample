@extends('admin.master')
@section('css')
@endsection
@section('js')
    <script>
        $('#oldPassword').keyup(function () {
            var oldPassword = $(this).val();
            data = $('#formChangePassword').serialize();
            url = '{{route('admin.admin.viewChangePassword')}}';
            type = 'get';
            callApiFormData(data, url, type)
                .done(response => {
                    if(response.messages)
                    {
                        $('#btnSave').removeAttr('disabled');
                    }if(response.messages == false)
                    {
                        $('#btnSave').attr('disabled','disabled');
                    }
                })
        });
    </script>
@endsection
@section('content')

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>User</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Admin</a></li>
                    <li class="active"><a href="index.html">Thay đổi mật khẩu</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="container" id="registration-form">
                                <a href="index-2.html"><img src="assets/img/login-logo.png" class="login-logo"></a>
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2 mt-lg">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><h2>Thay đổi mật khẩu</h2></div>
                                            <div class="panel-body">
                                                @if(session('message'))
                                                    <div class="alert bg-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <span>×</span><span class="sr-only">Close</span></button>
                                                        <span
                                                            class="text-semibold">Well done!</span> {{session('message')}}
                                                    </div>
                                                @endif
                                                <form action="{{route('admin.admin.changePassword')}}" method="POST"
                                                      class="form-horizontal" enctype="multipart/form-data" id="formChangePassword">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="FullName" class="col-xs-4 control-label">Mật khẩu
                                                            cũ</label>
                                                        <div class="col-xs-8">
                                                            <input type="password" class="form-control" name="oldPassword"
                                                                   id="oldPassword" placeholder="Nhập mật khẩu cũ"
                                                                   value="">
                                                            @error('oldPassword')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-xs-4 control-label">Mật khẩu
                                                            mới</label>
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" name="newPassword"
                                                                   id="newPassword" placeholder="Nhập mật khẩu mới"
                                                                   value="{{old('newPassword')}}" autocomplete="off" >
                                                            @error('newPassword')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-xs-4 control-label">Nhập lại mật
                                                            khẩu</label>
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control"
                                                                   name="confirmPassword"
                                                                   id="confirmPassword" placeholder="Nhập lại mật khẩu"
                                                                   value="{{old('confirmPassword')}}" autocomplete="off">
                                                            @error('confirmPassword')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="clearfix">
                                                            <button type="submit" disabled
                                                                    class="btn btn-primary pull-right" id="btnSave">
                                                                Lưu
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>

@stop
