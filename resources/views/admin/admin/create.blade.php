@extends('admin.master')
@section('css')
    <style>
        .avatar {
            cursor: pointer;
        }
    </style>
@endsection
@section('js')
    <script>
        $('#avatarImage').click(function () {
            $('#avatar').click();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatarImage').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#avatar").change(function () {
            readURL(this);
            $('.close').css('display','block');
        });
        $('.close').click(function () {
            $('#avatar').val('');
            $('#avatarImage').attr('src','assets/img/no-avatar.jpg');
        });
    </script>
@endsection
@section('content')

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>User</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Admin</a></li>
                    <li class="active"><a href="index.html">Thêm mới</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="container" id="registration-form">
                                <a href="index-2.html"><img src="assets/img/login-logo.png" class="login-logo"></a>
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2 mt-lg">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><h2>Form đăng ký</h2></div>
                                            <div class="panel-body">
                                                @if(session('message'))
                                                    <div class="alert bg-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <span>×</span><span class="sr-only">Close</span></button>
                                                        <span
                                                            class="text-semibold">Well done!</span> {{session('message')}}
                                                    </div>
                                                @endif
                                                <form action="{{route('admin.admin.store')}}" method="POST"
                                                      class="form-horizontal" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="FullName" class="col-xs-4 control-label">Full
                                                            Name</label>
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" name="name"
                                                                   id="FullName" placeholder="Full Name"
                                                                   value="{{old('name')}}">
                                                            @error('name')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Username"
                                                               class="col-xs-4 control-label">Username</label>
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" name="username"
                                                                   id="Username" placeholder="Username"
                                                                   value="{{old('username')}}">
                                                            @error('username')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="height: 200px">
                                                        <label for="Username"
                                                               class="col-xs-4 control-label">Avatar</label>
                                                        <button id="close" type="button" class="close" style="display: none"><span>×</span><span
                                                                class="sr-only">Close</span></button>
                                                        <div class="col-xs-8">
                                                            <div class="avatar">
                                                                <img src="{{asset('assets/img/no-avatar.jpg')}}" alt=""
                                                                     style="height: 190px" id="avatarImage" name="avatarImage">

                                                            </div>
                                                            <input type="file" class="form-control hidden"
                                                                   name="avatar"
                                                                   id="avatar">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Email" class="col-xs-4 control-label">Email</label>
                                                        <div class="col-xs-8">
                                                            <input type="email" class="form-control" name="email"
                                                                   id="Email" placeholder="Email"
                                                                   value="{{old('email')}}">
                                                            @error('email')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Password"
                                                               class="col-xs-4 control-label">Password</label>
                                                        <div class="col-xs-8">
                                                            <input type="password" class="form-control" name="password"
                                                                   id="Password" placeholder="Password">
                                                            @error('password')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ConfirmPassword" class="col-xs-4 control-label">Confirm
                                                            -Password</label>
                                                        <div class="col-xs-8">
                                                            <input type="password" class="form-control"
                                                                   name="confirmPassword" id="ConfirmPassword"
                                                                   placeholder="Confirm Password">
                                                            @error('confirmPassword')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="clearfix">
                                                            <button type="submit" class="btn btn-primary pull-right">
                                                                Đăng ký
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>

@stop
