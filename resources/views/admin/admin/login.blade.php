<!DOCTYPE html>
<html lang="en" class="coming-soon">

<!-- Mirrored from avalon.redteamux.com/extras-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 Oct 2014 07:53:56 GMT -->
<head>
    <meta charset="utf-8">
    <title>Login Form</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="">
    <meta name="author" content="The Red Team">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet'
          type='text/css'>
    <link href="{{asset('assets/plugins/iCheck/skins/minimal/blue.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('assets/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('assets/css/styles.css')}}" type="text/css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
<!--[if lt IE 9]>
    <link href="{{asset('assets/css/ie8.css')}}" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The following CSS are included as plugins and can be removed if unused-->

</head>

<body class="focused-form">
<script>
</script>

<div class="container" id="login-form">
    <a href=""><img src="{{asset('assets/img/login-logo.png')}}" class="login-logo"></a>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Login Form</h2></div>
                <div class="panel-body">
                    <form action="{{route('login')}}" method="POST" class="form-horizontal">
                        @csrf
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-user"></i>
									</span>
                                    <input type="text" class="form-control" placeholder="Email Username" name="email">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-key"></i>
									</span>
                                    <input type="password" class="form-control" id="exampleInputPassword1"
                                           placeholder="Password" name="password">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="{{route('password.request')}}" class="pull-left">Forgot password?</a>
                                <div class="checkbox-inline icheck pull-right pt0">
                                    <label for="">
                                        <input type="checkbox"></input>
                                        Remember me
                                    </label>
                                </div>
                            </div>
                        </div>


                        <div class="panel-footer">
                            <div class="clearfix">
{{--                                <a href="extras-registration.html" class="btn btn-default pull-left">Register</a>--}}
                                <button type="submit" class="btn btn-primary pull-right">Login</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Load site level scripts -->

<script src="{{asset('assets/js/jquery-1.10.2.min.js')}}"></script>                            <!-- Load jQuery -->
<script src="{{asset('assets/js/jqueryui-1.9.2.min.js')}}"></script>                            <!-- Load jQueryUI -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>                                <!-- Load Bootstrap -->

<script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<!-- Slimscroll for custom scrolls -->
<script src="{{asset('assets/plugins/sparklines/jquery.sparklines.min.js')}}"></script>        <!-- Sparkline -->
<script src="{{asset('assets/plugins/jstree/dist/jstree.min.js')}}"></script>                <!-- jsTree -->

<script src="{{asset('assets/plugins/codeprettifier/prettify.js')}}"></script>                <!-- Code Prettifier  -->
<script src="{{asset('assets/plugins/bootstrap-switch/bootstrap-switch.js')}}"></script>
<!-- Swith/Toggle Button -->

<script src="{{asset('assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js')}}"></script>
<!-- Bootstrap Tabdrop -->

<script src="{{asset('assets/plugins/iCheck/icheck.min.js')}}"></script>                        <!-- iCheck -->

<script src="{{asset('assets/js/enquire.min.js')}}"></script>
<!-- Enquire for Responsiveness -->

<script src="{{asset('assets/plugins/bootbox/bootbox.js')}}"></script>                    <!-- BOOTBOX -->

<script src="{{asset('assets/js/application.js')}}"></script>
<script src="{{asset('assets/demo/demo.js')}}"></script>
<script src="{{asset('assets/demo/demo-switcher.js')}}"></script>

<script src="{{asset('assets/plugins/simpleWeather/jquery.simpleWeather.min.js')}}"></script>

<!-- End loading site level scripts -->
<!-- Load page level scripts-->

<!-- End loading page level scripts-->
</body>

<!-- Mirrored from avalon.redteamux.com/extras-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 Oct 2014 07:53:56 GMT -->
</html>
