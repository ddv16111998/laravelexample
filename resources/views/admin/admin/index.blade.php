@extends('admin.master')
@section('css')
    <style>
        #formUpdateAdmin .form-control {
            margin-bottom: 20px;
        }
    </style>
@endsection
@section('js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function getHtmlTableAdmin(data) {
            var htmlTable = '';
            var stt = 1;
            data.forEach(value => {
                htmlTable += '<tr class="admin_'+value.id+'"><td>' + value.id + '</td><td>' + value.name + '</td><td>' + value.username + '</td><td>' + value.email + '</td><td>' + "Hoạt động" + '</td><td><button class="btn btn-primary btnEditModalAdmin" data-toggle="modal" data-target="#updateModalAdmin" data-id="' + value.id + '">Sửa</button><button class="btn btn-danger btnDeleteAdmin" data-toggle="modal" data-id="'+value.id+'">Xóa</button></td></tr>';
            });
            $('tbody').html(htmlTable);
        }
        function getHtmlByEdit(data) {
            var html = `<td>${data.id}</td><td>${data.name}</td><td>${data.username}</td><td>${data.email}</td><td>Hoạt động</td><td><button class="btn btn-primary btnEditModalAdmin" data-toggle="modal" data-target="#updateModalAdmin" data-id="${data.id}">Sửa</button><button class="btn btn-danger btnDeleteAdmin" data-toggle="modal" data-target="#deleteModalAdmin"
                                                                    data-target="#deleteModalAdmin"
                                                                    data-id="${data.id}">Xóa</button></td>`;
            $('.admin_'+data.id).html(html);
        }
        function deleteHtmlByEdit(data) {
            $('.admin_'+data).html("");
        }

        function resetErrorAdmin() {
            $('.emailError').html("");
            $('.nameError').html("");
            $('.usernameError').html("");
        }

        $('#searchAdmin').keyup(function () {
            $('.adminsPagination').css('display', 'none');
            dataSearch = $('#formAdmin').serialize();
            urlResource = '{{route('admin.admin.search')}}';
            type = 'get';
            callApi(dataSearch, urlResource, type)
                .done(response => {
                    getHtmlTableAdmin(response.admins);
                })
        });

        $('body').on('click', '.btnEditModalAdmin', function () {
            resetErrorAdmin();
            idResource = $(this).attr('data-id');
            urlResource = 'admin/admin/edit/' + idResource;
            type = 'get';
            callApi(data = null, urlResource, type)
                .done(response => {
                    $('#nameEdit').val(response.admin.name);
                    $('#usernameEdit').val(response.admin.username);
                    $('#emailEdit').val(response.admin.email);
                    $('#idAdminEdit').val(response.admin.id);
                    response.admin.avatar ? $('#avatarImageEdit').attr('src', 'assets/img/admin/' + response.admin.avatar) : $('#avatarImageEdit').attr('src', 'assets/img/no-avatar.jpg');
                    response.admin.status == 1 ? $('#active').attr('selected', 'selected') : $('#disActive').attr('selected', 'selected');
                })
        });

        $('#btnUpdateResourceAdmin').click(function (event) {
            event.preventDefault();
            dataResource = new FormData($('#formUpdateAdmin')[0]);
            urlResource = 'admin/admin/update/' + idResource;
            type = 'post';
            callApiFormData(dataResource, urlResource, type)
                .done(response => {
                    // console.log(response.admin.id);
                    getHtmlByEdit(response.admin);
                    $('#updateModalAdmin').modal('hide');
                    swal("Thành công!", "Bạn đã cập nhật thành công!", "success");
                })
                .fail(errors => {
                    $('.emailError').html((errors.responseJSON.errors.email));
                    $('.nameError').html((errors.responseJSON.errors.name));
                    $('.usernameError').html((errors.responseJSON.errors.username));
                })
        });
        $('body').on('click','.btnDeleteAdmin',function () {
            idResource = $(this).attr('data-id');
            urlResource = 'admin/admin/delete/' + idResource;
            type = 'delete';
            destroyConfirm(urlResource, type);

        });

        $('#avatarImageEdit').click(function () {
            $('#avatarEdit').click();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatarImageEdit').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#avatarEdit").change(function () {
            readURL(this);
            $('.close').css('display', 'block');
        });
        $('.close').click(function () {
            $('#avatarEdit').val('');
            $('#avatarImageEdit').attr('src', 'assets/img/no-avatar.jpg');
        });

    </script>

@endsection
@include('admin.admin.updateModal')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Admin</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Admin</a></li>
                    <li class="active"><a href="index.html">Danh sách</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="container">
                                <div class="row">
                                    @if(session('message'))
                                        <div class="alert bg-primary">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold">Well done!</span> {{session('message')}}
                                        </div>
                                    @endif
                                    @if(count($errors)>0)
                                        <div class="alert bg-danger">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold"><b>Lỗi!</b></span><br>
                                            @foreach($errors->all() as $err)
                                                {{$err}}<br>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="card">
                                        <div class="input-group well w100">
                                            <form action="" id="formAdmin">
                                                <input type="text" placeholder="Search..." class="form-control"
                                                       name="search" id="searchAdmin">
                                            </form>
                                        </div>

                                        <div class="card-body">
                                            <table id="listAdmin" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th style="width: 12px;">ID</th>
                                                    <th>Họ tên</th>
                                                    <th>Biệt danh</th>
                                                    <th>Email</th>
                                                    <th>Tình trạng</th>
                                                    <th>Hành động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($admins as $admin)
                                                    <tr class="admin_{{$admin->id}}">
                                                        <td>{{$admin->id}}</td>
                                                        <td>{{$admin->name}}</td>
                                                        <td>{{$admin->username}}</td>
                                                        <td>{{$admin->email}}</td>
                                                        <td>{{$admin->status == 1 ? "Hoạt động" : "Tạm khóa"}}</td>
                                                        <td>
                                                            <button class="btn btn-primary btnEditModalAdmin"
                                                                    data-toggle="modal"
                                                                    data-target="#updateModalAdmin"
                                                                    data-id="{{$admin->id}}">Sửa
                                                            </button>
                                                            <button class="btn btn-danger btnDeleteAdmin"
                                                                    data-toggle="modal"
                                                                    data-target="#deleteModalAdmin"
                                                                    data-id="{{$admin->id}}">Xóa
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="pull-right adminsPagination">{{$admins->links()}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection
