@extends('admin.master')
@include('admin.post.detailModal')
@section('css')
    <style>
        .avatarAdmin {
            border-radius: 50%;
        }

        #avatar {
            width: 300px;
            height: 300px;
            border-radius: 50%;
            position: relative;
            overflow: hidden;
        }

        #avatar:hover .uploadImg {
            display: block;
            cursor: pointer;

        }

        .uploadImg {
            position: absolute;
            bottom: 0px;
            left: 8px;
            width: 300px;
            height: 100px;
            background: #2b3c4e;
            margin: auto;
            display: none;
        }

        .uploadImg {
            text-align: center;
            padding-top: 40px;
        }
    </style>
@endsection
@section('js')
    <script>
        $('.uploadImg').click(function () {
            $('#avatarAdmin').click();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.avatarAdmin').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#avatarAdmin").change(function () {
            readURL(this);
            idResource = $('#avatarAdmin').attr('data-id');
            data = new FormData($('#formAvatar')[0]);
            url = 'admin/admin/updateAvatar/' + idResource;
            type = 'post';
            updateAvatar(data, url, type);
        });
        $('body').on('click', '.btnDeletePost', function () {
            idResource = $(this).attr('data-id');
            urlResource = 'admin/post/destroy/' + idResource;
            type = 'delete';
            destroyConfirm(urlResource, type);
        });
        $('body').on('click', '.detailPost', function () {
            idResource = $(this).attr('data-id');
            urlResource = 'admin/post/edit/' + idResource;
            type = 'get';
            callApi(data = null, urlResource, type)
                .done(response => {
                    // console.log(response);
                    $('#titleDetail').html(response.post.title);
                    $('#addressDetail').html(response.post.address);
                    $('#priceDetail').html(response.post.price);
                    $('#acreageDetail').html(response.post.acreage);
                    $('#categoryDetail').html(response.post.category.name);
                    $('#communeDetail').html(response.commune[0].name);
                    $('#districtDetail').html(response.commune[0].district.name);
                    var utilities = "";
                    response.utilities.forEach(value => utilities += value + " / ");
                    $('#utilityDetail').html(utilities);
                })
        });
    </script>
@endsection
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Thông tin cá nhân</h1>
                    <div class="options">
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="avatar">
                                <img class="avatarAdmin" src="{{\Auth::user()->avatar ? asset('assets/img/admin'.'/'.\Auth::user()->avatar) : asset('assets/img/no-avatar.jpg')}}"
                                     alt="" height="300" width="300">
                                <div class="uploadImg">
                                    <span><i class="fa fa-camera fa-lg"></i></span>
                                </div>
                                <form action="" id="formAvatar">
                                    @csrf
                                    <input type="file" class="hidden" name="avatar" id="avatarAdmin"
                                           data-id="{{\Auth::user()->id}}">
                                </form>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li><h3><b>Họ tên: </b>{{\Auth::user()->name}}</h3></li>
                                <li><h3><b>Biệt danh: </b>{{\Auth::user()->username}}</h3></li>
                                <li><h3><b>Email: </b>{{\Auth::user()->email}}</h3></li>
                                <li><h3><b>Quyền: </b>{{\Auth::user()->role == 0 ? "Quản trị viên" : "Người dùng"}}</h3>
                                </li>
                                <li><h3><b>Trạng thái: </b>{{\Auth::user()->status == 1 ? "Hoạt động" :"Tạm khóa"}}</h3>
                                </li>
                                <li><a href="{{route('admin.admin.viewChangePassword')}}" class="btn btn-danger">Đổi mật
                                        khẩu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Admin</a></li>
                    <li class="active"><a href="index.html">Danh sách bài viết</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="container">
                                <div class="row">
                                    @if(session('message'))
                                        <div class="alert bg-primary">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold">Well done!</span> {{session('message')}}
                                        </div>
                                    @endif

                                    @if(count($errors)>0)
                                        <div class="alert bg-danger">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold"><b>Lỗi!</b></span><br>
                                            @foreach($errors->all() as $err)
                                                {{$err}}<br>
                                            @endforeach
                                        </div>
                                    @endif

                                    <div class="card">
                                        <div class="card-body">
                                            <div id="panel-advancedoptions">
                                                <div class="row">
                                                    <!-- Begin Main -->
                                                    <div role="main" class="main pgl-bg-grey">
                                                        <!-- Begin Properties -->
                                                        <section class="pgl-properties pgl-bg-grey">
                                                            <div class="container">
                                                                <div
                                                                    class="properties-full properties-listing properties-listfull">
                                                                    @foreach(\Auth::user()->posts()->paginate(5) as $post)
                                                                        <div
                                                                            class="pgl-property animation post_{{$post->id}}">
                                                                            <div class="row">
                                                                                <div class="col-sm-4 col-md-4">
                                                                                    <div
                                                                                        class="property-thumb-info-image">
                                                                                        <img alt="" class="img"
                                                                                             src="{{ $post->images ? asset('assets/img/post').'/'.json_decode($post['images'])[0] : ""}}"
                                                                                             height="250px"
                                                                                             width="100%">
                                                                                        <span
                                                                                            class="property-thumb-info-label">
                                                                                            <span
                                                                                                class="label price"></span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-4 col-md-6">
                                                                                    <div class="property-thumb-info">
                                                                                        <div
                                                                                            class="property-thumb-info-content">
                                                                                            <h3>
                                                                                                <b>{{$post->title}}</b>
                                                                                            </h3>
                                                                                            <address><b>Địa
                                                                                                    điểm:</b> {{$post->address}}
                                                                                            </address>
                                                                                            <p><b>Thời gian
                                                                                                    đăng:</b> {{$post->created_at}}
                                                                                            </p>
                                                                                            <p><b>Danh
                                                                                                    mục:</b> {{$post->category->name}}
                                                                                            </p>
                                                                                            <strong>Diện
                                                                                                tích:</strong> {{$post->acreage}}
                                                                                            <sup>m2</sup>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-4 col-md-2 mt-lg">
                                                                                    <button
                                                                                        class="btn btn-info detailPost"
                                                                                        data-toggle="modal"
                                                                                        data-target="#detailModalPost"
                                                                                        data-id="{{$post->id}}">Chi
                                                                                        tiết
                                                                                    </button>
                                                                                    <button
                                                                                        class="btn btn-danger btnDeletePost"
                                                                                        data-id="{{$post->id}}">Xoá
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="adminInfomation">{{\Auth::user()->posts()->paginate(5)->links()}}</div>
                                                        </section>
                                                        <!-- End Properties -->
                                                    </div>
                                                    <!-- End Main -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection
