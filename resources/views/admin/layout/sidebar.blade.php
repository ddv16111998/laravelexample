<div class="static-sidebar-wrapper sidebar-default">
    <div class="static-sidebar">
        <div class="sidebar">


            <div class="widget stay-on-collapse">
                <div class="widget-body welcome-box tabular">
                    <div class="tabular-row">
                        <div class="tabular-cell welcome-avatar">
                            <a href="#"><img src="{{\Auth::user()->avatar ? asset('assets/img/admin').'/'.\Auth::user()->avatar : asset('assets/img/no-avatar.jpg')}}" class="avatar"></a>
                        </div>
                        <div class="tabular-cell welcome-options">
                            <span class="welcome-text">Welcome,</span>
                            <a href="{{route('admin.admin.information')}}" class="name">{{\Auth::user()->name}}</a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="widget stay-on-collapse" id="widget-sidebar">
                <span class="widget-heading">Explore</span>
                <nav role="navigation" class="widget-body">
                    <ul class="acc-menu">
                        <li><a href="{{route('admin.dashboard.index')}}"><i
                                    class="fa fa-home"></i><span>Dashboard</span><span
                                    class="badge badge-dark"></span></a></li>
                        <li><a href="javascript:;"><i class="fa fa-columns"></i><span>Admin</span><span
                                    class="badge badge-dark"></span></a>
                            <ul class="acc-menu">
                                <li><a href="{{route('admin.admin.index')}}">Danh sách</a></li>
                                <li><a href="{{route('admin.admin.create')}}">Thêm mới</a></li>
                            </ul>
                        </li>
{{--                        <li><a href="javascript:;"><i class="fa fa-users"></i><span>Người dùng</span><span--}}
{{--                                    class="badge badge-dark"></span></a>--}}
{{--                            <ul class="acc-menu">--}}
{{--                                <li><a href="">Danh sách</a></li>--}}
{{--                            </ul>--}}

{{--                        </li>--}}
                        <li><a href="javascript:;"><i class="fa fa-columns"></i><span>Bài đăng</span><span
                                    class="badge badge-dark"></span></a>
                            <ul class="acc-menu">
                                <li><a href="{{route('admin.post.create')}}">Đăng bài</a></li>
                                <li><a href="{{route('admin.post.index')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-columns"></i><span>Tiện ích</span><span
                                    class="badge badge-dark"></span></a>
                            <ul class="acc-menu">
                                <li><a href="{{route('admin.utility.create')}}">Thêm mới</a></li>
                                <li><a href="{{route('admin.utility.index')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-table"></i><span>Danh mục</span></a>
                            <ul class="acc-menu">
                                <li><a href="{{route('admin.category.index')}}">Danh sách danh mục</a></li>
{{--                                <li><a href="{{route('admin.category.create')}}">Thêm mới</a></li>--}}
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-flask"></i><span>Huyện</span></a>
                            <ul class="acc-menu">
                                <li><a href="{{route('admin.district.index')}}">Danh sách huyện</a></li>

                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-cogs"></i><span>Xã</span></a>
                            <ul class="acc-menu">
                                <li><a href="{{route('admin.commune.index')}}">Danh sách xã</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
