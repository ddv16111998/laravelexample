<div class="infobar-wrapper">
    <div class="infobar">

        <div class="infobar-options">
            <h2>Infobar</h2>
        </div>

        <div id="widgetarea">


            <div class="widget" id="widget-sparkline">
                <div class="widget-heading">
                    <a href="javascript:;" data-toggle="collapse" data-target="#sparklinestats"><h4>Sparkline Stats</h4></a>
                </div>
                <div class="widget-body collapse in" id="sparklinestats">
                    <ul class="sparklinestats">
                        <li>
                            <div class="pull-left">
                                <h5 class="title">Total Revenue</h5>
                                <h3>$241,750 <span class="badge badge-success">+13.6%</span></h3>
                            </div>
                            <div class="pull-right">
                                <div class="sparkline" id="infobar-revenuestats"></div>
                            </div>
                        </li>
                        <li>
                            <div class="pull-left">
                                <h5 class="title">Products Sold</h5>
                                <h3>11,562 <span class="badge badge-success">+19.2%</span></h3>
                            </div>
                            <div class="pull-right">
                                <div class="sparkline" id="infobar-unitssold"></div>
                            </div>
                        </li>
                        <li>
                            <div class="pull-left">
                                <h5 class="title">Total Orders</h5>
                                <h3>1,249 <span class="badge badge-danger">-10.5%</span></h3>
                            </div>
                            <div class="pull-right">
                                <div class="sparkline" id="infobar-orders"></div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="widget" id="widget-weather">
                <div class="widget-heading">
                    <a href="javascript:;" data-toggle="collapse" data-target="#weatherwidget"><h4>Weather</h4></a>
                </div>
                <div class="widget-body collapse in" id="weatherwidget">
                    <div class="weather-container">
                        <div class="weather-widget"></div>
                    </div>
                </div>
            </div>

            <div class="widget">
                <div class="widget-heading">
                    <a href="javascript:;" data-toggle="collapse" data-target="#recentactivity"><h4>Recent Activity</h4></a>
                </div>
                <div class="widget-body collapse in" id="recentactivity">
                    <ul class="recent-activities">
                        <li>
                            <div class="avatar">
                                <img src="assets/demo/avatar/avatar_11.png" class="img-responsive img-circle">
                            </div>
                            <div class="content">
                                <span class="msg"><a href="#" class="person">Jean Alanis</a> invited 3 unconfirmed members to <a href="#">Sed ut perspiciatis unde</a></span>
                                <span class="time">Sep 16, 2014 at 10:06 AM</span>

                            </div>
                        </li>
                        <li>
                            <div class="activityicon activity-success">
                                <i class="fa fa-cloud"></i>
                            </div>
                            <div class="content">
                                <span class="msg"><a href="#" class="person">Stacy Villani</a> and <a href="#" class="person">Leroy Greenlee</a> added new files to <a href="#">Dicta sunt explicabo</a></span>
                                <span class="time">Sep 12, 2014 at 11:06 PM</span>

                            </div>
                        </li>
                        <li>
                            <div class="avatar">
                                <img src="assets/demo/avatar/avatar_07.png" class="img-responsive img-circle">
                            </div>
                            <div class="content">
                                <span class="msg"><a href="#" class="person">Shannon Schmucker</a> is now following <a href="#" class="person">Anthony Ware</a></span>
                                <span class="time">Sep 06, 2014 at 1:46 AM</span>
                            </div>
                        </li>
                        <li>
                            <div class="avatar">
                                <img src="assets/demo/avatar/avatar_01.png" class="img-responsive img-circle">
                            </div>
                            <div class="content">
                                <span class="msg"><a href="#" class="person">Roxann Hollingworth</a> commented on <a href="#">Natus error sit voluptatem</a></span>
                                <span class="time">Sep 02, 2014 at 7:50 PM</span>
                            </div>
                        </li>
                        <li>
                            <div class="avatar">
                                <img src="assets/demo/avatar/avatar_04.png" class="img-responsive img-circle">
                            </div>
                            <div class="content">
                                <span class="msg"><a href="#" class="person">Mitchell Kosak</a> added <a href="#" class="person">Bruce Ory</a> to <a href="#">Accusantium doloremque laudantium</a></span>
                                <span class="time">Sep 02, 2014 at 8:35 AM</span>
                            </div>
                        </li>
                        <li>
                            <div class="activityicon activity-inverse">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="content">
                                <span class="msg"><a href="#">4 new users</a> requested to join group</span>
                                <span class="time">Aug 29, 2014 at 05:34 PM</span>

                            </div>
                        </li>
                        <li>
                            <div class="avatar">
                                <img src="assets/demo/avatar/avatar_09.png" class="img-responsive img-circle">
                            </div>
                            <div class="content">
                                <span class="msg"><a href="#" class="person">Rodney Moody</a> created new thread <a href="#">Vero eos et accusamus</a></span>
                                <span class="time">Aug 13, 2014 at 1:23 PM</span>

                            </div>
                        </li>
                        <li>
                            <div class="activityicon activity-info">
                                <i class="fa fa-comment-o"></i>
                            </div>
                            <div class="content">
                                <span class="msg">Anonymous user commented on <a href="#">Totam rem aperiam</a></span>
                                <span class="time">Aug 11, 2014 at 12:01 PM</span>

                            </div>
                        </li>
                        <li>
                            <div class="avatar">
                                <img src="assets/demo/avatar/avatar_05.png" class="img-responsive img-circle">
                            </div>
                            <div class="content">
                                <span class="msg"><a href="#" class="person">Pricilla Panella</a> is now following <a href="#" class="person">Ricky Marengo</a></span>
                                <span class="time">Jul 25, 2014 at 3:11 PM</span>
                            </div>
                        </li>
                        <li class="seeall">
                            <a href="#" class="pull-right">See all activities</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
