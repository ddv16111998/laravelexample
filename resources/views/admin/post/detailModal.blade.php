<!-- The Modal -->
<div class="modal" id="detailModalPost">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Chi tiết bài đăng</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <label for="title" class="col-sm-4 control-label">Tiều đề</label>
                    <div class="col-sm-8">
                        <div id="titleDetail">

                        </div>
                    </div>
                </div><br>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Địa chỉ</label>
                    <div class="col-sm-8">
                        <div id="addressDetail">

                        </div>
                    </div>
                </div><br>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Giá</label>
                    <div class="col-sm-8">
                        <div id="priceDetail">

                        </div>
                    </div>
                </div><br>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Diện tích</label>
                    <div class="col-sm-8">
                        <div id="acreageDetail">

                        </div>
                    </div>
                </div><br>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Danh mục</label>
                    <div class="col-sm-8">
                        <div id="categoryDetail">

                        </div>
                    </div>
                </div><br>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Xã</label>
                    <div class="col-sm-8">
                        <div id="communeDetail">

                        </div>
                    </div>
                </div><br>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Huyện</label>
                    <div class="col-sm-8">
                        <div id="districtDetail">

                        </div>
                    </div>
                </div><br>
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Tiện ích</label>
                    <div class="col-sm-8">
                        <div id="utilityDetail">

                        </div>
                    </div>
                </div><br>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
