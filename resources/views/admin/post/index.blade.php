@extends('admin.master')
@section('css')
@endsection
@section('js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('body').on('click', '.detailPost', function () {
            idResource = $(this).attr('data-id');
            urlResource = 'admin/post/edit/' + idResource;
            type = 'get';
            callApi(data = null, urlResource, type)
                .done(response => {
                    $('#titleDetail').html(response.post.title);
                    $('#addressDetail').html(response.post.address);
                    $('#priceDetail').html(response.post.price);
                    $('#acreageDetail').html(response.post.acreage);
                    $('#categoryDetail').html(response.post.category.name);
                    $('#communeDetail').html(response.commune[0].name);
                    $('#districtDetail').html(response.commune[0].district.name);
                    var utilities = "";
                    response.utilities.forEach(value => utilities += value + " / ");
                    $('#utilityDetail').html(utilities);
                })
        });

        $('body').on('click', '.btnDeletePost', function () {
            swal({
                title: "Bạn có muốn xóa?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        idResource = $(this).attr('data-id');
                        urlResource = 'admin/post/destroy/' + idResource;
                        type = 'delete';
                        callApiFormData(data = null, urlResource, type)
                            .done(responve => {
                                if (responve.messages == 'Success') {
                                    $('.post_' + idResource).html("");
                                    swal("Xóa thành công!", "You clicked the button!", "success");
                                } else {
                                    swal("Bạn không có quyền xóa!", "You clicked the button!", "auth");
                                }
                            });
                    }
                });
        });

        function getHtmlSearchPost(data) {
            var htmlPost = '';
            data.forEach(value => {
                htmlPost += `<div class="pgl-property animation post_${value.id}">
                                    <div class="row">
                                        <div class="col-sm-4 col-md-4">
                                            <div
                                                class="property-thumb-info-image">
                                                <img alt="" class="img"
                                                     src="assets/img/post/${JSON.parse(value.images)}"
                                                     height="250px"
                                                     width="100%">
                                                <span
                                                    class="property-thumb-info-label">
                                                    <span
                                                        class="label price"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-6">
                                            <div class="property-thumb-info">
                                                <div
                                                    class="property-thumb-info-content">
                                                    <h3>
                                                        <a href="property-detail.html"><b>${value.title}</b></a>
                                                    </h3>

                                                    <address><b>Địa
                                                            điểm:</b> ${value.address}
                                                    </address>
                                                    <p><b>Thời gian
                                                            đăng:</b> ${value.created_at}
                                                    </p>
                                                    <p><b>Danh
                                                            mục:</b> ${value.category.name}
                                                    </p>
                                                    <strong>Diện
                                                        tích:</strong> ${value.acreage}
                                                    <sup>m2</sup><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-2 mt-lg">
                                            <button
                                                class="btn btn-info detailPost"
                                                data-toggle="modal"
                                                data-target="#detailModalPost"
                                                data-id="${value.id}">Chi
                                                        tiết
                                                    </button>
                                                    <button
                                                        class="btn btn-danger btnDeletePost"
                                                        data-id="${value.id}">Xoá
                                                    </button>
                                                </div>
                                            </div>
                                        </div><hr>`;
            });
            $('.properties-listfull').html(htmlPost);
        }

        $('#searchPost').keyup(function () {
            dataSearch = $('#formSearchPost').serialize();
            urlResource = '{{route('admin.post.search')}}';
            type = 'get';
            $('.postPagination').css('display', 'none');
            callApi(dataSearch, urlResource, type)
                .done(response => {
                    getHtmlSearchPost(response);
                })
        });

    </script>
@endsection
@include('admin.post.detailModal')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Danh sách bài viết </h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Admin</a></li>
                    <li class="active"><a href="index.html">Danh sách bài viết</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp
                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <hr>
                            <div class="container">
                                <div class="row">
                                    @if(session('message'))
                                        <div class="alert bg-primary">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold">Well done!</span> {{session('message')}}
                                        </div>
                                    @endif
                                    <div class="card">
                                        <div class="input-group well w100">
                                            <form action="" method="" id="formSearchPost">
                                                <input type="text" placeholder="Search..." class="form-control"
                                                       name="search" id="searchPost">
                                            </form>
                                        </div>

                                        <div class="card-body">
                                            <div id="panel-advancedoptions">
                                                <div class="row">
                                                    <!-- Begin Main -->
                                                    <div role="main" class="main pgl-bg-grey">
                                                        <!-- Begin Properties -->
                                                        <section class="pgl-properties pgl-bg-grey">
                                                            <div class="container">
                                                                <div
                                                                    class="properties-full properties-listing properties-listfull">
                                                                    @foreach($posts as $post)
                                                                        <div
                                                                            class="pgl-property animation post_{{$post->id}}">
                                                                            <div class="row">
                                                                                <div class="col-sm-4 col-md-4">
                                                                                    <div
                                                                                        class="property-thumb-info-image">
                                                                                        <img alt="" class="img"
                                                                                             src="{{ $post->images ? asset('assets/img/post').'/'.json_decode($post['images'])[0] : ""}}"
                                                                                             height="250px"
                                                                                             width="100%">
                                                                                        <span
                                                                                            class="property-thumb-info-label">
                                                                                            <span
                                                                                                class="label price"></span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-4 col-md-6">
                                                                                    <div class="property-thumb-info">
                                                                                        <div
                                                                                            class="property-thumb-info-content">
                                                                                            <h3>
                                                                                                <a href="property-detail.html"><b>{{$post->title}}</b></a>
                                                                                            </h3>

                                                                                            <address><b>Địa
                                                                                                    điểm:</b> {{$post->address}}
                                                                                            </address>
                                                                                            <p><b>Thời gian
                                                                                                    đăng:</b> {{$post->created_at}}
                                                                                            </p>
                                                                                            <p><b>Danh
                                                                                                    mục:</b> {{$post->category->name}}
                                                                                            </p>
                                                                                            <strong>Diện
                                                                                                tích:</strong> {{$post->acreage}}
                                                                                            <sup>m2</sup><br>
                                                                                            <b>Người đăng
                                                                                                :</b> {{$post->user()->first()->name}}
                                                                                            <br>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-4 col-md-2 mt-lg">
                                                                                    <button
                                                                                        class="btn btn-info detailPost"
                                                                                        data-toggle="modal"
                                                                                        data-target="#detailModalPost"
                                                                                        data-id="{{$post->id}}">Chi
                                                                                        tiết
                                                                                    </button>
                                                                                    <button
                                                                                        class="btn btn-danger btnDeletePost"
                                                                                        data-id="{{$post->id}}">Xoá
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                                <div class="postPagination">
                                                                    {{$posts->links()}}
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <!-- End Properties -->
                                                    </div>
                                                    <!-- End Main -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection





