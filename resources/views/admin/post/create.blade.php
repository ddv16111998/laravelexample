@extends('admin.master')
@section('css')
@endsection
@section('js')
    <script type="text/javascript">
        $('#file-image').fileinput({
            theme: 'fa',
            language: 'vi',
            showUpload: false,
            allowedFileExtensions: ['jpg', 'png', 'gif']
        });

        $('#district_id').change(function () {
            var district_id = $(this).val();
            $.ajax({
                type: "GET",
                url: "{{route('getCommuneByDistrict')}}",
                dataType: "html",
                data: {district_id: district_id},
            })
                .done(function (data) {
                    $('#commune_id').html(data);
                })
        });
        $('.utilitySelect').selectpicker();
    </script>
@endsection
@section('content')
    @php

    @endphp
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Đăng bài</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Bài đăng</a></li>
                    <li class="active"><a href="index.html">Đăng bài</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="container-fluid">
                                <div class="row">
                                    @if(session('message'))
                                        <div class="alert bg-primary">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold">Well done!</span> {{session('message')}}
                                        </div>
                                    @endif
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="container-fluid" id="post-form">
                                                <a href="index-2.html"><img src="assets/img/login-logo.png"
                                                                            class="login-logo"></a>
                                                <div class="row">
                                                    <div class="col-md-8 col-md-offset-2 mt-lg">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading"><h2>Bài đăng</h2></div>
                                                            <div class="panel-body">
                                                                @if(session('message'))
                                                                    <div class="alert bg-success">
                                                                        <button type="button" class="close"
                                                                                data-dismiss="alert"><span>×</span><span
                                                                                class="sr-only">Close</span></button>
                                                                        <span
                                                                            class="text-semibold">Well done!</span> {{session('message')}}
                                                                    </div>
                                                                @endif
                                                                <form action="{{route('admin.post.store')}}"
                                                                      method="POST" class="form-horizontal"
                                                                      enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <label for="FullName"
                                                                               class="col-xs-4 control-label">Tiêu
                                                                            đề</label>
                                                                        <div class="col-xs-8">
                                                                            <input type="text" class="form-control"
                                                                                   name="title" id=""
                                                                                   placeholder="Nhập tiêu đề"
                                                                                   value="{{old('title')}}">
                                                                            @error('title')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="address"
                                                                               class="col-xs-4 control-label">Địa
                                                                            chỉ</label>
                                                                        <div class="col-xs-8">
                                                                            <input type="text" class="form-control"
                                                                                   name="address" id="address"
                                                                                   placeholder="Nhập địa chỉ"
                                                                                   value="{{old('address')}}">
                                                                            @error('address')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="price"
                                                                               class="col-xs-4 control-label">Giá
                                                                            tiền</label>
                                                                        <div class="col-xs-8">
                                                                            <input type="text" class="form-control"
                                                                                   name="price" id="price"
                                                                                   placeholder="Nhập giá tiền"
                                                                                   value="{{old('price')}}">
                                                                            @error('price')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="acreage"
                                                                               class="col-xs-4 control-label">Diện
                                                                            tích</label>
                                                                        <div class="col-xs-8">
                                                                            <input type="text" class="form-control"
                                                                                   name="acreage" id="acreage"
                                                                                   placeholder="Nhập diện tích"
                                                                                   value="{{old('acreage')}}">
                                                                            @error('acreage')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="district_id"
                                                                               class="col-xs-4 control-label">Huyện /
                                                                            Phường </label>
                                                                        <div class="col-xs-8">
                                                                            <select name="district_id" id="district_id"
                                                                                    class="form-control">
                                                                                <option value=""> -- Chọn huyện / phường
                                                                                    --
                                                                                </option>
                                                                                @foreach($districts as $district)
                                                                                    <option
                                                                                        value="{{$district->id}}">{{$district->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @error('district_id')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="commune_id"
                                                                               class="col-xs-4 control-label">Xã</label>
                                                                        <div class="col-xs-8">
                                                                            <select name="commune_id" id="commune_id"
                                                                                    class="form-control">
                                                                                <option value=""> -- Chọn xã --</option>
                                                                            </select>
                                                                            @error('commune_id')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="cate_id"
                                                                               class="col-xs-4 control-label">Danh
                                                                            mục </label>
                                                                        <div class="col-xs-8">
                                                                            <select name="category_id" id="category_id"
                                                                                    class="form-control">
                                                                                <option value=""> -- Chọn danh mục --
                                                                                </option>
                                                                                @foreach($categories as $category)
                                                                                    <option
                                                                                        value="{{$category->id}}">{{$category->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @error('category_id')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="image"
                                                                               class="col-xs-4 control-label">Ảnh </label>
                                                                        <div class="col-xs-8">
                                                                            <input id="file-image" type="file"
                                                                                   class="file" name="images[]" multiple
                                                                                   data-preview-file-type="any"
                                                                                   data-upload-url="#">
                                                                            @error('images')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="image"
                                                                               class="col-xs-4 control-label">Tiện ích </label>
                                                                        <div class="col-xs-8">
                                                                            <select class="utilitySelect form-control" name="utilities[]" multiple>
                                                                                @foreach($utilities as $utility)
                                                                                    <option value="{{$utility->id}}">{{$utility->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="phone"
                                                                               class="col-xs-4 control-label">Số điện
                                                                            thoại </label>
                                                                        <div class="col-xs-8">
                                                                            <input type="text" class="form-control"
                                                                                   name="phone" id="phone"
                                                                                   placeholder="Nhập số điện thoại"
                                                                                   value="{{old('phone')}}">
                                                                            @error('phone')
                                                                            <div
                                                                                class="text-danger">{{ $message }}</div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="description"
                                                                               class="col-xs-4 control-label">Ghi
                                                                            chú</label>
                                                                        <div class="col-xs-8">
                                                                            <textarea class="form-control" id="editor1"
                                                                                      name="description">
                                                                                {{old('description')}}
                                                                            </textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-footer">
                                                                        <div class="clearfix">
                                                                            <button type="submit"
                                                                                    class="btn btn-primary pull-right">
                                                                                Đăng bài
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection

