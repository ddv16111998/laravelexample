@extends('admin.master')

@section('css')

@endsection
@section('js')
    <script>
        // function category
        function showErrorCategory(errors) {
            errors.name ? $('.nameError').html(errors.name[0]) : "";
        }

        function resetErrorCategory() {
            $('.nameError').html("");
        }
        function loadHtmlTableCategory(data) {
                var html = `<tr class="category_${data.id}"><td>${data.id}</td><td>${data.name}</td><td>${data.slug}</td><td><button class="btn btn-primary btnEditCateModal" data-toggle="modal" data-target="#updateCateModal" data-id="${data.id}" data-content="${data.name}">Sửa</button><button class="btn btn-danger btnDeleteCate" data-id="${data.id}">Xóa</button></td></tr>`;
                $('tbody').append(html);
        }

        function getHtmlTableCategory(data) {
            var htmlTable = '';
            var stt = 1;
            data.forEach(value => {
                htmlTable += '<tr class="category_'+value.id+'"><td>' + value.id + '</td><td>' + value.name + '</td><td>' + value.slug + '</td><td><button class="btn btn-primary btnEditCateModal" data-toggle="modal" data-target="#updateCateModal" data-id="' + value.id + '" data-content="' + value.name + '">Sửa</button><button class="btn btn-danger btnDeleteCate" data-id="' + value.id + '">Xóa</button></td></tr>';
            });
            $('tbody').html(htmlTable);
        }

        function updateHtmlTableCategory(data)
        {
            var html = `<td>${data.id}</td><td>${data.name}</td><td>${data.slug}</td><td><button class="btn btn-primary btnEditCateModal" data-toggle="modal" data-target="#updateCateModal" data-id="${data.id}" data-content="${data.name}">Sửa</button><button class="btn btn-danger btnDeleteCate" data-id="${data.id}">Xóa</button></td>`;
            $('.category_'+data.id).html(html);
        }
        //end function cate


        $(function () {
            //setup ajax
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // event js cate
            $('#btnNewCateModal').click(function () {
                resetErrorCategory();
            });
            $('#name').keyup(function () {
                resetErrorCategory();
            });

            $('#btnNewResourceCate').click(function (event) {
                event.preventDefault();
                dataResource = $('#newResourceCateForm').serialize();
                urlResource = 'admin/category/store';
                type = 'post';
                callApi(dataResource, urlResource, type)
                    .done(response => {
                        $('#newCateModal').modal('hide');
                        swal("Thành công!", "Bạn đã thêm mới thành công!", "success");
                        $('.swal-button--confirm').click(function () {
                            loadHtmlTableCategory(response.category);
                        })
                    })
                    .fail(error => {
                        const errors = error.responseJSON.errors;
                        showErrorCategory(errors);
                    })
            });
            $('body').on('click', '.btnEditCateModal', function () {
                resetErrorCategory();
                idResource = $(this).attr('data-id');
                $('#idCategory').val(idResource);
                nameEdit = $(this).attr('data-content');
                $('#updateCateModal #nameEdit').val(nameEdit);
            });

            $('#btnUpdateResourceCate').click(function (event) {
                event.preventDefault();
                dataResource = $('#updateResourceCateForm').serialize();
                urlResource = 'admin/category/update/' + idResource;
                type = 'put';
                callApi(dataResource, urlResource, type)
                    .done(response => {
                        $('#updateCateModal').modal('hide');
                        updateHtmlTableCategory(response.category);
                        swal("Thành công!", "Bạn đã cập nhật thành công!", "success");
                    })
                    .fail(error => {
                        const errors = error.responseJSON.errors;
                        showErrorCategory(errors);
                    });
            });

            $('body').on('click', '.btnDeleteCate', function (event) {
                idResource = $(this).attr('data-id');
                urlResource = 'admin/category/destroy/' + idResource;
                type = 'delete';
                destroyConfirm(urlResource, type);
            });

            $('#searchCategory').keyup(function () {
                dataSearch = $('#formSearchCate').serialize();
                urlResource = 'admin/category/search';
                type = 'get';
                $('.categoryPagination').css('display','none');
                callApi(dataSearch, urlResource, type)
                    .done(response => {
                        getHtmlTableCategory(response.data);
                    })
            });


        });

    </script>
@endsection
@include('admin.category.addModal')
@include('admin.category.updateModal')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Danh mục</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Danh mục</a></li>
                    <li class="active"><a href="index.html">Danh sách</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#newCateModal"
                                    id="btnNewCateModal">Add++
                            </button>
                            <div class="container">
                                <div class="row">
                                    @if(session('message'))
                                        <div class="alert bg-primary">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold">Well done!</span> {{session('message')}}
                                        </div>
                                    @endif

                                    @if(count($errors)>0)
                                        <div class="alert bg-danger">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold"><b>Lỗi!</b></span><br>
                                            @foreach($errors->all() as $err)
                                                {{$err}}<br>
                                            @endforeach
                                        </div>
                                    @endif

                                    <div class="card">
                                        <div class="input-group well w100">
                                            <form action="" id="formSearchCate">
                                                <input type="text" placeholder="Search..." class="form-control"
                                                       name="search" id="searchCategory">
                                            </form>
                                        </div>

                                        <div class="card-body">
                                            <table id="listCategory" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th style="width: 12px;">ID</th>
                                                    <th>Danh mục</th>
                                                    <th>Đường dẫn</th>
                                                    <th>Hành động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($categories as $category)
                                                    <tr class="category_{{$category->id}}">
                                                        <td>{{$category->id}}</td>
                                                        <td>{{$category->name}}</td>
                                                        <td>{{$category->slug}}</td>
                                                        <td>
                                                            <button class="btn btn-primary btnEditCateModal"
                                                                    data-id="{{$category->id}}" data-toggle="modal"
                                                                    data-target="#updateCateModal"
                                                                    data-content="{{$category->name}}">Sửa
                                                            </button>
                                                            <button class="btn btn-danger btnDeleteCate"
                                                                    data-id="{{$category->id}}">Xóa
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="categoryPagination">{{$categories->links()}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection
