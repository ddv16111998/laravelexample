<!--modal update -->
<div class="modal fade" id="updateCateModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Cập nhật danh mục</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="" data-url="" method="POST" id="updateResourceCateForm">
                    @csrf
                    <input type="text" class="hidden" id="idCategory" name="id">
                    <div class="form-group">
                        <label for="name" class="col-xs-4 control-label">Tên</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="nameEdit" name="name" placeholder="Nhập danh mục">
                            <div class="nameError" style="color: red">

                            </div>
                        </div>

                    </div>

                    <div class="footer">
                        <button type="submit" class="btn btn-primary" id="btnUpdateResourceCate">Lưu</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
