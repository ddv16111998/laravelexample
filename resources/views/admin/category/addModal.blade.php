<!--modal add -->
<div class="modal fade" id="newCateModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Thêm mới danh mục</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="" data-url="" method="POST" id="newResourceCateForm">
                    @csrf
                    <div class="form-group">
                        <label for="name" class="col-xs-4 control-label">Tên</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Nhập danh mục">
                            <div class="nameError" style="color: red">

                            </div>
                        </div>

                    </div>

                    <div class="footer">
                        <button type="submit" class="btn btn-primary" id="btnNewResourceCate">Lưu</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
