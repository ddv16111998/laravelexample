@extends('admin.master')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Danh mục</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Danh mục</a></li>
                    <li class="active"><a href="index.html">Thêm mới</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="container" id="registration-form">
                                <a href="index-2.html"><img src="{{asset('assets/img/login-logo.png')}}" class="login-logo"></a>
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2 mt-lg">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><h2>Tạo mới</h2></div>
                                            <div class="panel-body">
                                                @if(session('message'))
                                                    <div class="alert bg-danger">
                                                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                                        <span class="text-semibold"></span>  {{session('message')}}
                                                    </div>
                                                @endif
                                                <form action="{{route('admin.category.store')}}" method="POST" class="form-horizontal">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="category" class="col-xs-4 control-label">Tên danh mục</label>
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" name="name" id="name" placeholder="Tên danh mục" value="{{old('name')}}">
                                                            @error('name')
                                                            <div class="text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="clearfix">
                                                            <button type="submit" class="btn btn-primary pull-right">Tạo mới</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>

@stop
