<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Admin</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{asset('')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="Avalon Admin Theme">
    <meta name="author" content="The Red Team">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,700' rel='stylesheet' type='text/css'>

    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
{{--<!--[if lt IE 10]>--}}
{{--    <script src="{{asset('assets/js/media.match.min.js')}}"></script>--}}
{{--    <script src="{{asset('assets/js/placeholder.min.js')}}"></script>--}}
{{--    <![endif]-->--}}

    <link href="{{asset('assets/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('assets/css/styles.css')}}" type="text/css" rel="stylesheet">

    <link href="{{asset('assets/plugins/jstree/dist/themes/avalon/style.min.css')}}" type="text/css" rel="stylesheet">
{{--    <link href="{{asset('assets/plugins/codeprettifier/prettify.css')}}" type="text/css" rel="stylesheet">--}}
{{--    <link href="{{asset('assets/plugins/iCheck/skins/minimal/blue.css')}}" type="text/css" rel="stylesheet">--}}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
<!--[if lt IE 9]>
    <link href="{{asset('assets/css/ie8.css')}}" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
  <script type="text/javascript" src=""></script>-->
{{--    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>--}}
{{--    <![endif]-->--}}
    <!-- The following CSS are included as plugins and can be removed if unused-->

    <link href="{{asset('assets/plugins/form-daterangepicker/daterangepicker-bs3.css')}}" type="text/css" rel="stylesheet">    	<!-- DateRangePicker -->
    <link href="{{asset('assets/plugins/switchery/switchery.css')}}" type="text/css" rel="stylesheet">        					<!-- Switchery -->
    <link href="{{asset('assets/plugins/fullcalendar/fullcalendar.css')}}" type="text/css" rel="stylesheet">
    <!-- FullCalendar -->

    <!--File input-->
    <link rel="stylesheet" href="{{asset('assets/file-input/fileinput.css')}}">

    <link rel="stylesheet" href="{{asset('assets/js/bootstrap-select.min.css')}}">

    @yield('css')
</head>

<body class="infobar-offcanvas">

@include('admin.layout.header')

<div id="wrapper">
    <div id="layout-static">
        @include('admin.layout.sidebar')

        @yield('content')
    </div>
</div>







<!-- Load site level scripts -->
{{--<script src="{{asset('assets/js/script-admin.js')}}"></script>--}}

<script src="{{asset('assets/js/bootstrap.min.js')}}"></script> 								<!-- Load Bootstrap -->
<!--end DataTable-->
<script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script> 	<!-- Slimscroll for custom scrolls -->
<script src="{{asset('assets/plugins/sparklines/jquery.sparklines.min.js')}}"></script>  		<!-- Sparkline -->
<script src="{{asset('assets/plugins/jstree/dist/jstree.min.js')}}"></script>  				<!-- jsTree -->

<script src="{{asset('assets/plugins/codeprettifier/prettify.js')}}"></script> 				<!-- Code Prettifier  -->
<script src="{{asset('assets/plugins/bootstrap-switch/bootstrap-switch.js')}}"></script> 		<!-- Swith/Toggle Button -->

<script src="{{asset('assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js')}}"></script>  <!-- Bootstrap Tabdrop -->

<script src="{{asset('assets/plugins/iCheck/icheck.min.js')}}"></script>     					<!-- iCheck -->

<script src="{{asset('assets/js/enquire.min.js')}}"></script> 										<!-- Enquire for Responsiveness -->

{{--<script src="{{asset('assets/plugins/bootbox/bootbox.js')}}"></script>					<!-- BOOTBOX -->--}}

<script src="{{asset('assets/js/application.js')}}"></script>
<script src="{{asset('assets/demo/demo.js')}}"></script>
{{--<script src="{{asset('assets/demo/demo-switcher.js')}}"></script>--}}

<script src="{{asset('assets/plugins/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
{{----}}
<!-- End loading site level scripts -->

<!-- Load page level scripts-->

{{--<script src="{{asset('assets/plugins/form-daterangepicker/daterangepicker.js')}}"></script>     	<!-- Date Range Picker -->--}}
{{--<script src="{{asset('assets/plugins/form-daterangepicker/moment.min.js')}}"></script>             <!-- Moment.js for Date Range Picker -->--}}

{{--<script src="{{asset('assets/plugins/easypiechart/jquery.easypiechart.js')}}"></script> 			<!-- EasyPieChart -->--}}
{{--<script src="{{asset('assets/plugins/powerwidgets/js/powerwidgets.js')}}"></script> 				<!-- PowerWidgets -->--}}
{{--<script src="{{asset('assets/plugins/switchery/switchery.js')}}"></script>     					<!-- Switchery -->--}}

{{--<script src="{{asset('assets/plugins/fullcalendar/fullcalendar.min.js')}}"></script>   			<!-- FullCalendar -->--}}

<!-- Charts -->
{{--<script src="{{asset('assets/plugins/charts-flot/jquery.flot.min.js')}}"></script>             	<!-- Flot Main File -->--}}
{{--<script src="{{asset('assets/plugins/charts-flot/jquery.flot.stack.min.js')}}"></script>       	<!-- Flot Stacked Charts Plugin -->--}}
{{--<script src="{{asset('assets/plugins/charts-flot/jquery.flot.orderBars.min.js')}}"></script>   	<!-- Flot Ordered Bars Plugin-->--}}
{{--<script src="{{asset('assets/plugins/charts-flot/jquery.flot.resize.min.js')}}"></script>          <!-- Flot Responsive -->--}}
{{--<script src="{{asset('assets/plugins/charts-flot/jquery.flot.tooltip.min.js')}}"></script> 		<!-- Flot Tooltips -->--}}

{{--<!-- Maps -->--}}
{{--<script src="{{asset('assets/plugins/jQuery-Mapael/js/raphael/raphael-min.js')}}"></script>        <!-- Load Raphael as Dependency -->--}}
{{--<script src="{{asset('assets/plugins/jQuery-Mapael/js/jquery.mapael.js')}}"></script>              <!-- jQuery Mapael -->--}}

{{--<script src="{{asset('assets/plugins/jQuery-mousewheel/jquery.mousewheel.min.js')}}"></script>    <!-- Mousewheel Support in zoomed-in maps -->--}}
{{--<script src="{{asset('assets/plugins/jQuery-Mapael/js/maps/world_countries.js')}}"></script>       <!-- Vector Data of World Countries -->--}}

{{--<!--ckeditor js-->--}}
{{--<script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>--}}
{{--<script> CKEDITOR.replace('editor1'); </script>--}}
{{--<script> CKEDITOR.replace('editor_edit'); </script>--}}

{{--<script src="{{asset('assets/demo/demo-index.js')}}"></script> 									<!-- Initialize scripts for this page-->--}}

<!-- End loading page level scripts-->
<script src="{{asset('assets/file-input/js/fileinput.js')}}"></script>
<script src="{{asset('assets/file-input/js/vi.js')}}"></script>
<script src="{{asset('assets/js/script-admin.js')}}"></script>
<script src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-select.min.js')}}"></script>
@yield('js')

</body>
</html>
