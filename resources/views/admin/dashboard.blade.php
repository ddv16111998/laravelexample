@extends('admin.master')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Dashboard</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">


                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="col-md-3">
                                <a class="info-tiles tiles-inverse has-footer" href="#">
                                    <div class="tiles-heading">
                                        <div class="pull-left">Admin</div>
                                        <div class="pull-right">
                                            <div id="tileorders" class="sparkline-block"></div>
                                        </div>
                                    </div>
                                    <div class="tiles-body">
                                        <div class="text-center">{{$admins->count()}}</div>
                                    </div>
                                    <div class="tiles-footer">
                                        <div class="pull-left">manage orders</div>
                                        <div class="pull-right percent-change">+20.7%</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a class="info-tiles tiles-green has-footer" href="#">
                                    <div class="tiles-heading">
                                        <div class="pull-left">Số bài đăng</div>
                                        <div class="pull-right">
                                            <div id="tilerevenues" class="sparkline-block"></div>
                                        </div>
                                    </div>
                                    <div class="tiles-body">
                                        <div class="text-center">{{$posts->count()}}</div>
                                    </div>
                                    <div class="tiles-footer">
                                        <div class="pull-left">go to accounts</div>
                                        <div class="pull-right percent-change">+17.2%</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a class="info-tiles tiles-blue has-footer" href="#">
                                    <div class="tiles-heading">
                                        <div class="pull-left">Số danh mục</div>
                                        <div class="pull-right">
                                            <div id="tiletickets" class="sparkline-block"></div>
                                        </div>
                                    </div>
                                    <div class="tiles-body">
                                        <div class="text-center">{{$categories->count()}}</div>
                                    </div>
                                    <div class="tiles-footer">
                                        <div class="pull-left">see all tickets</div>
                                        <div class="pull-right percent-change">+10.3%</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-3">
                                <a class="info-tiles tiles-midnightblue has-footer" href="#">
                                    <div class="tiles-heading">
                                        <div class="pull-left">Số tiện ích</div>
                                        <div class="pull-right">
                                            <div id="tilemembers" class="sparkline-block"></div>
                                        </div>
                                    </div>
                                    <div class="tiles-body">
                                        <div class="text-center">{{$utilities->count()}}</div>
                                    </div>
                                    <div class="tiles-footer">
                                        <div class="pull-left">manage members</div>
                                        <div class="pull-right percent-change">-11.1%</div>
                                    </div>
                                </a>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 bs-grid">
                                <div class="panel panel-default panel-btn-focused" id="p1"
                                     data-widget-editbutton="false">
                                    <div class="panel-heading">
                                        <h2>
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#tab-visitor" data-toggle="tab"><i
                                                            class="fa fa-user visible-xs"></i><span class="hidden-xs">Đăng nhiều nhất</span></a>
                                                </li>
                                                <li><a href="#tab-revenues" data-toggle="tab"><i
                                                            class="fa fa-bar-chart-o visible-xs"></i><span
                                                            class="hidden-xs">Revenues</span></a></li>
                                            </ul>
                                        </h2>
                                    </div>
                                    <div class="panel-colorbox" style="display: none">
                                        <ul class="list-unstyled list-inline panel-color-list">
                                            <li><span data-widget-setstyle="panel-default"></span></li>
                                            <li><span data-widget-setstyle="panel-inverse"></span></li>
                                            <li><span data-widget-setstyle="panel-primary"></span></li>
                                            <li><span data-widget-setstyle="panel-success"></span></li>
                                            <li><span data-widget-setstyle="panel-warning"></span></li>
                                            <li><span data-widget-setstyle="panel-danger"></span></li>
                                            <li><span data-widget-setstyle="panel-info"></span></li>
                                            <li><span data-widget-setstyle="panel-brown"></span></li>
                                            <li><span data-widget-setstyle="panel-indigo"></span></li>
                                            <li><span data-widget-setstyle="panel-orange"></span></li>
                                            <li><span data-widget-setstyle="panel-midnightblue"></span></li>
                                            <li><span data-widget-setstyle="panel-sky"></span></li>
                                            <li><span data-widget-setstyle="panel-magenta"></span></li>
                                            <li><span data-widget-setstyle="panel-purple"></span></li>
                                            <li><span data-widget-setstyle="panel-green"></span></li>
                                            <li><span data-widget-setstyle="panel-grape"></span></li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="col-md-12 bs-grid">
                                                <div class="panel panel-default panel-btn-focused demo-new-members" id="p3">
                                                    <div class="panel-heading">
                                                        <h2>TOP 10</h2>
                                                    </div>
                                                    <div class="panel-editbox" style="display: none">
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <input type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-colorbox" style="display: none">
                                                        <ul class="list-unstyled list-inline panel-color-list">
                                                            <li><span data-widget-setstyle="panel-default"></span></li>
                                                            <li><span data-widget-setstyle="panel-inverse"></span></li>
                                                            <li><span data-widget-setstyle="panel-primary"></span></li>
                                                            <li><span data-widget-setstyle="panel-success"></span></li>
                                                            <li><span data-widget-setstyle="panel-warning"></span></li>
                                                            <li><span data-widget-setstyle="panel-danger"></span></li>
                                                            <li><span data-widget-setstyle="panel-info"></span></li>
                                                            <li><span data-widget-setstyle="panel-brown"></span></li>
                                                            <li><span data-widget-setstyle="panel-indigo"></span></li>
                                                            <li><span data-widget-setstyle="panel-orange"></span></li>
                                                            <li><span data-widget-setstyle="panel-midnightblue"></span></li>
                                                            <li><span data-widget-setstyle="panel-sky"></span></li>
                                                            <li><span data-widget-setstyle="panel-magenta"></span></li>
                                                            <li><span data-widget-setstyle="panel-purple"></span></li>
                                                            <li><span data-widget-setstyle="panel-green"></span></li>
                                                            <li><span data-widget-setstyle="panel-grape"></span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="panel-body panel-no-padding">
                                                        <div class="tabel-responsive">
                                                            <table class="table table-hover mb0">
                                                                <thead>
                                                                <tr>

                                                                    <th width="25">Họ tên</th>
                                                                    <th width="45" class="hidden-xs">Email</th>
                                                                    <th width="30">Số bài đăng</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($postsOfUser as $post)
                                                                    @foreach($post[0]->user()->get() as $value)
                                                                    <tr>
                                                                        <td>{{$value->name}}</td>
                                                                        <td class="hidden-xs">{{$value->email}}</td>
                                                                        <td>{{count($post)}}</td>
                                                                    </tr>
                                                                    @endforeach
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection
