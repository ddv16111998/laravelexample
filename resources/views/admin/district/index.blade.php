@extends('admin.master')
@section('css')
@endsection
@section('js')
    <script>
        function getHtmlTableDistrict(data) {
            var htmlTable = '';
            var stt = 1;
            data.forEach(value => {
                htmlTable += '<tr><td>' + (stt++) + '</td><td>' + value.name + '</td><td>' + value.slug + '</td><td><button class="btn btn-primary" disabled>Sửa</button><button class="btn btn-danger" disabled>Xóa</button></td></tr>';
                // htmlTable += `<tr><td>${stt}</td><td>${value.name}</td>`;
            });
            $('tbody').html(htmlTable);
        }

        $('#searchDistrict').keyup(function () {
            $('.paginate').css("display","none");
            dataSearch = $('#formDistrict').serialize();
            console.log(dataSearch);
            urlResource = '{{route('admin.district.search')}}';
            type = 'get';
            callApi(dataSearch, urlResource, type)
                .done(response => {
                    getHtmlTableDistrict(response.districts);
                })
        });
    </script>
@endsection
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Tỉnh thành</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">

                    <li class="active"><a href="">Tỉnh</a></li>
                    <li class="active"><a href="index.html">Danh sách</a></li>

                </ol>
                @php
                    $stt = 1;
                @endphp

                <div class="container-fluid">
                    <div id="panel-advancedoptions">
                        <div class="row">
                            <div class="container">
                                <div class="row">
                                    @if(session('message'))
                                        <div class="alert bg-primary">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                    class="sr-only">Close</span></button>
                                            <span class="text-semibold">Well done!</span> {{session('message')}}
                                        </div>
                                    @endif
                                    <div class="card">
                                        <div class="input-group well w100">
                                            <form action="" id="formDistrict">
                                                <input type="text" placeholder="Search..." class="form-control"
                                                       name="search" id="searchDistrict">
                                            </form>
                                        </div>

                                        <div class="card-body">
                                            <table id="listDistrict" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th style="width: 12px;">STT</th>
                                                    <th>Huyện</th>
                                                    <th>Đường dẫn</th>
                                                    <th>Hành động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($districts as $district)
                                                    <tr>
                                                        <td>{{$stt++}}</td>
                                                        <td>{{$district->name}}</td>
                                                        <td>{{$district->slug}}</td>
                                                        <td>
                                                            <button class="btn btn-primary" data-toggle="modal"
                                                                    data-target="edit" disabled>Sửa
                                                            </button>
                                                            <button class="btn btn-danger" data-toggle="modal"
                                                                    data-target="#delete" disabled>Xóa
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="pull-right paginate">{{ $districts->links() }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2014 Avalon</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                        class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection

