<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix' => 'admin', 'namespace' => 'Admin','middleware'=>'checkLoginAdmin'], function () {
    Route::get('dashboard', 'DashBoardController@index')->name('admin.dashboard.index');
    Route::group(['prefix' => 'admin'], function () {
        Route::get('index','AdminController@index')->name('admin.admin.index');
        Route::get('create','AdminController@create')->name('admin.admin.create');
        Route::post('store','AdminController@store')->name('admin.admin.store');
        Route::post('update/{id}','AdminController@update')->name('admin.admin.update');
        Route::get('edit/{id}','AdminController@edit')->name('admin.admin.edit');
        Route::post('updateAvatar/{id}','AdminController@updateAvatar')->name('admin.admin.updateAvatar');
        Route::delete('delete/{id}','AdminController@destroy')->name('admin.admin.destroy');
        Route::get('search','AdminController@search')->name('admin.admin.search');
        Route::get('information','AdminController@information')->name('admin.admin.information');
        Route::get('changePassword','AdminController@viewChangePassword')->name('admin.admin.viewChangePassword');
        Route::post('changePassword','AdminController@changePassword')->name('admin.admin.changePassword');

    });
    Route::group(['prefix' => 'category'], function () {
        Route::get('index', 'CategoryController@index')->name('admin.category.index');
//        Route::get('create', 'CategoryController@create')->name('admin.category.create');
        Route::post('store', 'CategoryController@store')->name('admin.category.store');
        Route::put('update/{id}', 'CategoryController@update')->name('admin.category.update');
        Route::get('search', 'CategoryController@search')->name('admin.category.search');
        Route::delete('destroy/{id}', 'CategoryController@destroy')->name('admin.category.destroy');
    });
    Route::group(['prefix' => 'district'], function () {
        Route::get('index', 'DistrictController@index')->name('admin.district.index');
        Route::get('search', 'DistrictController@search')->name('admin.district.search');
    });
    Route::group(['prefix' => 'commune'], function () {
        Route::get('index', 'CommuneController@index')->name('admin.commune.index');
        Route::get('search', 'CommuneController@search')->name('admin.commune.search');
    });
    Route::group(['prefix'=>'post'],function(){
        Route::get('index','PostController@index')->name('admin.post.index');
        Route::get('create','PostController@create')->name('admin.post.create');
        Route::post('store','PostController@store')->name('admin.post.store');
        Route::get('edit/{id}','PostController@edit')->name('admin.post.edit');
        Route::get('search','PostController@search')->name('admin.post.search');
        Route::delete('destroy/{id}','PostController@destroy')->name('admin.post.destroy');
    });
    Route::group(['prefix'=>'utility'],function (){
        Route::get('index','UtilityController@index')->name('admin.utility.index');
        Route::get('create','UtilityController@create')->name('admin.utility.create');
        Route::post('store','UtilityController@store')->name('admin.utility.store');
        Route::get('edit/{id}','UtilityController@edit')->name('admin.utility.edit');
        Route::post('update/{id}','UtilityController@update')->name('admin.utility.update');
        Route::get('destroy/{id}','UtilityController@destroy')->name('admin.utility.destroy');
        Route::get('search','UtilityController@search')->name('admin.utility.search');
    });

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/getLocation', 'HomeController@getCommuneByDistrict')->name('getCommuneByDistrict');
