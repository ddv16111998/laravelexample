var idResource;
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//function global
function callApi(data, url, type, dataType = 'json') {
    return $.ajax({
        type: type,
        url: url,
        dataType: dataType,
        data: data,
    })
}

function callApiFormData(data, url, type, dataType = 'json') {
    return $.ajax({
        type: type,
        url: url,
        dataType: dataType,
        data: data,
        contentType: false,
        processData: false,
    })
}

function destroyConfirm(url, type) {
    return swal({
        title: "Bạn có muốn xóa?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                callApiFormData(data = null, url, type)
                    .done(responve => {
                        $('.'+responve.module+'_' + responve.id).html("");
                    });
                swal("Bạn đã xóa thành công!", {
                    icon: "success",
                })
            } else {
                swal("Xóa không thành công!");
            }
        });
}

function updateAvatar(data, url, type) {
    swal({
        title: "Bạn có muốn thay đổi ảnh đại diện?",
        icon: "info",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                callApiFormData(data, url, type);
                swal("Bạn đã thay đổi ảnh đại diện thành công!", {
                    icon: "success",
                });
            } else {
                swal("Bạn không thay đổi ảnh đại diện");
            }
        });
}

//end function global

