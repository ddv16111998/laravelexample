//Variable

const DELETE_STATUS_CODE = 204;
const SUCCESS_STATUS_CODE = 200;
const CREATE_STATUS_CODE = 201;

//function
function isSuccess(status) {
    return status == SUCCESS_STATUS_CODE;
}

function isCreated(status) {
    return status == CREATE_STATUS_CODE;
}

function isDeleted(status) {
    return status == DELETE_STATUS_CODE;
}


function alertSuccess(message) {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: message,
        showConfirmButton: true,
        confirmButtonText: 'ok'
    })
        .then((result) => {
            if (result.value) {
                location.reload();
            }
        })

}

function alertError(message) {
    Swal.fire({
        icon: 'error',
        title: 'Oops...:',
        text: message,
    })
}
//curd resource
function getResource( url) {
    return $.ajax({
        url: url,
        type: "get",
    });
}

function newResource(data, url) {
    return $.ajax({
        url: url,
        type: "post",
        data: data
    });
}

function updateResource(data, url) {
    return $.ajax({
        url: url,
        type: "PUT",
        data: data
    });
}

function deleteResource(id, url) {
    return $.ajax(
        {
            url: url + id,
            type: 'delete',
            dataType: "JSON",
            data: {"id": id,}
        });
}

function searchResource(data, url) {
    return $.ajax(
        {
            url: url,
            type: 'post',
            dataType: "JSON",
            data: data
        });
}

function destroyResource(id, url) {
    Swal.fire({
        title: 'Xác nhận xóa?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xóa ',
        cancelButtonText: 'Hủy bỏ'
    }).then((result) => {
        if (result.value) {
            deleteResource(id, url)
                .done(response => {
                    if (isDeleted(response.status)) {
                        alertSuccess(response.message);
                    } else {
                        aleartError(error.message);
                    }
                })
                .fail(error => {
                    aleartError(error.message);
                });
        }
    });
}

function fillDataToTableHtml(data) {
    let tableHTML = "";
    data.forEach(item => {
        tableHTML += "<tr>";
        tableHTML += "<td>" + item.name + "<td>";
        tableHTML += "<td>" + item.description + "<td>";
        tableHTML += "</tr>";
    });
    return tableHTML;
}

function loadDataPaginate(response) {

}
function resetError() {
    $('.nameError').html("");
    $('.descriptionError').html('');
}

function showError(errors) {
    (errors.name) ? $('.nameError').html(errors.name[0]) : "";
    (errors.description) ? $('.descriptionError').html(errors.description[0]) : "";
}

//jquery
$(function () {
    //set up ajax
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //variable

    //id of action resource
    let idActionResource = 0;
    let urlResource="";
    //data of action resource
    let dataResource = "";

    let btnBrandSearch = $('.brandSearch');
    //brand
    let newBrand = $('.newBrand');
    let updateBrand = $('.updateBrand');
    //btn
    let deleteBtnResource = $('.deleteResource');
    let editBtnResource = $('.editBtnResource');
    let addBtnResource = $('.addBtnResource');
//form resource

    let editResourceForm = $('.editResourceForm');
    let newResourceForm = $('.newResourceForm');

    addBtnResource.click(function () {
        resetError();
    })
    editBtnResource.click(function (event) {
        idActionResource = $(this).attr('editId');
        // resetError();
    });

    updateBrand.click(function (event) {
        event.preventDefault();
        dataResource = editResourceForm.serialize();
        urlResource='/api/manage/brand/' + idActionResource;
        updateResource(dataResource, urlResource)
            .done(response => {
                $('#newBrandModal').modal('hide')
                isSuccess(response.status) ? alertSuccess(response.message) : "";
            })
            .fail(error => {
                const errors = error.responseJSON.errors;
                showError(errors);
            })
    });


    newBrand.click(function (event) {
        event.preventDefault();
        dataResource = newResourceForm.serialize();
        urlResource='/api/manage/brand';
        newResource(dataResource, urlResource)
            .done(response => {
                $('#newBrandModal').modal('hide');
                isSuccefss(response.status) ? alertSuccess(response.message) : "";
            })
            .fail(error => {
                const errors = error.responseJSON.errors;
                showError(errors);

            });
    });
    deleteBtnResource.click(function (event) {
        event.preventDefault();

        idActionResource = $(this).attr('deleteId');
        urlResource="/api/manage/brand/";
        destroyResource(idActionResource,urlResource );
    });


    btnBrandSearch.click(function (e) {
        e.preventDefault();
        dataResource = $('#BrandFormRearch').serialize();
        urlResource='/api/manage/brand/search';
        searchResource(dataResource, urlResource)
            .done(data => {
                const brands = data.data;
                // $('.table-data').html(fillDataToTableHtml(brands))
                console.log(brands);
            })
            .fail(error => {
                showError(error.message);
            });
    });


    $('#editModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var name = button.data('name')
        var description = button.data('description')
        var modal = $(this)
        modal.find('.modal-body #name').val(name);
        modal.find('.modal-body #description').val(description);

    })


});
