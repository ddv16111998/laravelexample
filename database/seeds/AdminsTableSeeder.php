<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(
            [
                [
                    'name'  => 'develop',
                    'username' => 'Develop',
                    'avatar' => null,
                    'email' => 'develop@gmail.com',
                    'password' => bcrypt('123456'),
                    'role'  => 0,
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name'  => 'admin',
                    'username' => 'admin',
                    'avatar' => null,
                    'email' => 'admin@gmail.com',
                    'password' => bcrypt('123456'),
                    'role'  => 0,
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]
        );
    }
}
