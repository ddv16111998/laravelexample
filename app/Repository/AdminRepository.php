<?php

namespace App\Repository;

use App\User;
use Illuminate\Http\Request;
use App\Repository\ImageRepository;

class AdminRepository extends ImageRepository
{
    public function getAllAdmin()
    {
        return User::where(['role' => 0])->paginate(10);
    }

    public function getAdminById($id)
    {
        return User::findOrFail($id);
    }

    public function updateAvatar($id, Request $request)
    {
        if ($request->file('avatar')) {
            $admin = $id ? $this->getAdminById($id) : null;
            $data['avatar'] = $this->uploadOneImage($request, 'avatar', 'assets/img/admin');
            $result = User::updateOrCreate(['id' => $id], $data);
            $this->destroyImage($request, $admin, 'avatar', 'assets/img/admin');
            return $result;
        }
    }

    public function updateOrCreateAdmin($id, Request $request)
    {
        $admin = $id ? $this->getAdminById($id) : null;
        $data = $request->except('password');
        $data['avatar'] = $request->file('avatar') ? $this->uploadOneImage($request, 'avatar', 'assets/img/admin') : null;
        $data['password'] = $id ? $admin->password : bcrypt($request->password);
        $data['role'] = 0;
        if ($id && $data['avatar'] == null) {
            $data['avatar'] = $admin['avatar'];
        }
        $result = User::updateOrCreate(['id' => $id], $data);
        $this->destroyImage($request, $admin, 'avatar', 'assets/img/admin');
        return $result;
    }

    public function destroyAdmin($id)
    {
        return User::destroy($id);
    }

    public function searchAdmin($data)
    {
        return User::where(['role' => 0])->where('name', 'like', '%' . $data . '%')->orWhere('username', 'like', '%' . $data . '%')->orWhere('email', 'like', '%' . $data . '%')->get();
    }
}
