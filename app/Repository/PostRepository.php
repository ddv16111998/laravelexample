<?php

namespace App\Repository;

use App\Post;
use Illuminate\Http\Request;
use App\Repository\ImageRepository;

class PostRepository extends ImageRepository
{
    public function getAllPost()
    {
        return Post::orderBy('created_at', 'DESC')->paginate(5);
    }

    public function updateOrCreatePost($id, Request $request)
    {
        $data = $request->except('images', 'district_id', 'utilities');
        if ($request->file('images')) {
            $data['images'] = $this->uploadMultiImage($request, 'images', 'assets/img/post');
        }
        $data['user_id'] = \Auth::user()->id;
        $data['description'] = strip_tags($data['description']);
        $rel_data = $request->utilities;
        $post = Post::updateOrCreate(['id' => $id], $data);
        if ($id) {
            $post->utilities()->detach();
        }
        if ($post->id && $rel_data != null) {
            foreach ($rel_data as $key => $val) {
                $post->utilities()->attach($post->id, [
                    'utility_id' =>$val
                ]);
            }
        }
    }

    public function searchPost($data)
    {
        return Post::where('title', 'like', '%' . $data . '%')->with('category')->get();
    }

    public function groupByPostOfUser()
    {
        return Post::all()->groupBy('user_id');

    }
}
