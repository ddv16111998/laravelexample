<?php

namespace App\Repository;

use App\Commune;
use Illuminate\Http\Request;

class CommuneRepository
{
    public function getAllCommunes()
    {
        return Commune::all();
    }

    public function getAllPaginationCommunes()
    {
        return Commune::paginate(10);
    }

    public function getCommunesByDistrict($id_district)
    {
        return Commune::where('district_id', $id_district)->get();
    }
    public function search(Request $request)
    {
        $dataSearch = $request->search;
        $communes = Commune::where('name','like','%'.$dataSearch.'%')->orWhere('slug','like','%'.$dataSearch.'%')->with('district')->get();
        return response()->json([
            'communes'=>$communes
        ],200);
    }
}
