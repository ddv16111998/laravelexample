<?php

namespace App\Repository;

use App\Utility;
use Illuminate\Http\Request;

class UtilityRepository
{
    public function getPaginateUtilities()
    {
        return Utility::paginate(5);
    }
    public  function getAllUtilies()
    {
        return Utility::all();
    }

    public function getUtilityById($id)
    {
        return Utility::findOrFail($id);
    }

    public function updateOrCreateUtility($id, Request $request)
    {
        $data['name'] = $request->name;
        return Utility::updateOrCreate(['id' => $id], $data);
    }

    public function destroy($id)
    {
        return Utility::destroy($id);
    }
    public function search(Request $request)
    {
        return Utility::where('name','like','%'.$request->search.'%')->get();
    }
}
