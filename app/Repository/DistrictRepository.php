<?php
namespace App\Repository;
use App\District;
use Illuminate\Http\Request;

class  DistrictRepository
{
    public function getAllDistrict()
    {
        return District::all();
    }

    public function getAllPaginateDistrict()
    {
        return District::paginate(10);
    }
    public function search(Request $request)
    {
        $dataSearch = $request->search;
        $districts = District::where('name','like','%'.$dataSearch.'%')->orWhere('slug','like','%'.$dataSearch.'%')->get();
        return response()->json([
            'districts'=>$districts
        ],200);
    }
}
