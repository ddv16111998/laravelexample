<?php
namespace App\Repository;
use App\Category;
use Illuminate\Http\Request;

class CategoryRepository{

    public function getAllCategory()
    {
        return Category::paginate(5);
    }

    public function updateOrCreateCategory(Request $request,$id)
    {
        $name = $request->name;
        $slug = $slug = \Str::slug($name);
        $data = [
            'name' => $name,
            'slug' => $slug
        ];
        return Category::updateOrCreate(['id' => $id], $data);
    }

    public function destroyCategory($id)
    {
        return Category::destroy($id);
    }

    public function searchCategory(Request $request)
    {
        $dataSearch = $request->search;
        return Category::where('name', 'like', '%' . $dataSearch . '%')->orWhere('slug', 'like', '%' . $dataSearch . '%')->get();
    }
}
