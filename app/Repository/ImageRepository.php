<?php

namespace App\Repository;

use Illuminate\Http\Request;

abstract class ImageRepository
{
    public function uploadOneImage(Request $request, $nameInput, $path)
    {
        $image = $request->file($nameInput)->getClientOriginalName().'-'.now();
        $request->file($nameInput)->move($path, $image.'-'.now());
        return $image.'-'.now();
    }

    public function uploadMultiImage(Request $request, $nameInput, $path)
    {
            foreach ($images = $request->file($nameInput) as $key => $image) {
                $data[$key] = $image->getClientOriginalName();
                $image->move($path, $data[$key]);
            }
            return json_encode($data);
    }

    public function destroyImage(Request $request,$module,$nameInput,$path)
    {
        if($module && $request->file($nameInput))
        {
            if($module[$nameInput] == null)
            {
                return;
            }
            unlink($path.'/'.$module[$nameInput]);
        }
    }
    public function destroyMultiImage($module,$nameInput,$path)
    {
        if($module)
        {
            foreach ($module[$nameInput] as $image)
            {
                unlink($path.'/'.$image);
            }
        }
    }
}
