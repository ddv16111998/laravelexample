<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\CommuneRepository;

class CommuneController extends Controller
{
    protected $communeRepository;

    function __construct(CommuneRepository $communeRepository)
    {
        $this->communeRepository = $communeRepository;
    }

    public function index()
    {
        $communes = $this->communeRepository->getAllPaginationCommunes();
        return view('admin.commune.index', ['communes' => $communes]);
    }
    public function search(Request $request)
    {
        return $this->communeRepository->search($request);
    }
}
