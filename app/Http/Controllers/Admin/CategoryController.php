<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\CategoryRepository;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $categories = $this->categoryRepository->getAllCategory();
        return view('admin.category.index', ['categories' => $categories]);
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(CategoryRequest $request)
    {
        $category = $this->categoryRepository->updateOrCreateCategory($request, $id = null);
        return response()->json([
            'category' => $category,
            200]);
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = $this->categoryRepository->updateOrCreateCategory($request, $id);
        return response()->json([
            'category' => $category
        ], 200);
    }

    public function search(Request $request)
    {
        $posts = $this->categoryRepository->searchCategory($request);
        return response()->json([
            'status' => 200,
            'messages' => 'Find data',
            'data' => $posts
        ], 200);
    }

    public function destroy($id)
    {
        $this->categoryRepository->destroyCategory($id);
        return response()->json([
            'id' => $id,
            'module' => 'category',
        ], 200);
    }

//    public function store(CategoryRequest $request)
//    {
//        $category = $this->categoryRepository->updateOrCreateCategory($request, $id = null);
//        $request->session()->flash('message', 'Tạo mới thành công');
//        return redirect()->route('admin.category.index');
//    }
}
