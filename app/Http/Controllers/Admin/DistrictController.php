<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\DistrictRepository;

class DistrictController extends Controller
{
    protected $districtRepository;

    function __construct(DistrictRepository $districtRepository)
    {
        $this->districtRepository = $districtRepository;
    }

    public function index()
    {
        $districts = $this->districtRepository->getAllPaginateDistrict();
        return view('admin.district.index', ['districts' => $districts]);
    }
    public function search(Request $request)
    {
        return $this->districtRepository->search($request);
    }

}
