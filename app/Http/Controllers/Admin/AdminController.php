<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use App\Repository\AdminRepository;
use App\Http\Requests\EditAdminRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangePasswordRequest;

class AdminController extends Controller
{
    protected $adminRepository;

    function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    public function index()
    {
        $admins = $this->adminRepository->getAllAdmin();
        return view('admin.admin.index', ['admins' => $admins]);
    }

    public function create()
    {
        return view('admin.admin.create');
    }

    public function store(AdminRequest $request)
    {
        $this->adminRepository->updateOrCreateAdmin($id = null, $request);
        $request->session()->flash('message', 'Thêm mới thành công!');
        return redirect()->route('admin.admin.index');
    }

    public function updateAvatar($id, Request $request)
    {
        return $this->adminRepository->updateAvatar($id, $request);
    }

    public function edit($id)
    {
        $admin = $this->adminRepository->getAdminById($id);
        return response()->json([
            'admin' => $admin
        ]);
    }

    public function update($id, EditAdminRequest $request)
    {
        $admin = $this->adminRepository->updateOrCreateAdmin($id, $request);
        return response()->json([
                'admin' => $admin,
            ]
        );
    }

    public function destroy($id)
    {
        $admin = User::destroy($id);
        return response()->json([
            'messages' => 'success',
            'id' => $id,
            'module' => 'admin'
        ], 200);
    }

    public function search(Request $request)
    {
        $admins = $this->adminRepository->searchAdmin($request->search);
        return response()->json([
            'admins' => $admins
        ], 200);
    }

    public function information()
    {
        return view('admin.admin.information');
    }

    public function viewChangePassword(Request $request)
    {
        if ($request->ajax()) {
            $oldPassword = $request->oldPassword;
            if (Hash::check($oldPassword, \Auth::user()->password)) {
                return response()->json([
                    'messages' => true,
                ]);
            }
            return response()->json([
                'messages' => false,
            ]);
        }
        return view('admin.admin.changePassword');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $password = $request->newPassword;
        $user = \Auth::user();
        $user->password = bcrypt($password);
        if ($user->save()) {
            return redirect()->back()->with('message', 'Thay đổi mật khẩu thành công');
        }
        return 'false';

    }
}
