<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\UtilityRepository;
use App\Http\Requests\UtilityRequest;

class UtilityController extends Controller
{
    protected $utilityRepository;

    function __construct(UtilityRepository $utilityRepository)
    {
        $this->utilityRepository = $utilityRepository;
    }

    public function index()
    {
        $utilities = $this->utilityRepository->getPaginateUtilities();
        return view('admin.utility.index', ['utilities' => $utilities]);
    }

    public function create()
    {
        return view('admin.utility.create');
    }

    public function store(UtilityRequest $request)
    {
        $this->utilityRepository->updateOrCreateUtility($id = null, $request);
        $request->session()->flash('message', 'Thêm mới thành công');
        return redirect()->route('admin.utility.index');
    }

    public function edit($id)
    {
        $utility = $this->utilityRepository->getUtilityById($id);
        return view('admin.utility.edit', ['utility' => $utility]);
    }
    public function update($id,UtilityRequest $request)
    {
        $this->utilityRepository->updateOrCreateUtility($id,$request);
        $request->session()->flash('message', 'Cập nhật thành công');
        return redirect()->route('admin.utility.index');
    }
    public function destroy($id,Request $request)
    {
        $this->utilityRepository->destroy($id);
        $request->session()->flash('message', 'Xoa thành công');
        return redirect()->route('admin.utility.index');
    }
    public function search(Request $request)
    {
        $dataSearch = $this->utilityRepository->search($request);
        return response()->json([
            'messages'=> 'success',
            'data' => $dataSearch,
        ]);
    }
}
