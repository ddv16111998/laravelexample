<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use App\Repository\PostRepository;
use App\Repository\DistrictRepository;
use App\Repository\CommuneRepository;
use App\Repository\CategoryRepository;
use App\Repository\UtilityRepository;

use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    protected $postRepository;

    function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        $posts = $this->postRepository->getAllPost();
        return view('admin.post.index', ['posts' => $posts]);
    }

    public function create(DistrictRepository $districtRepository, CommuneRepository $communeRepository, CategoryRepository $categoryRepository, UtilityRepository $utilityRepository)
    {
        $data['districts'] = $districtRepository->getAllDistrict();
        $data['communes'] = $communeRepository->getAllCommunes();
        $data['categories'] = $categoryRepository->getAllCategory();
        $data['utilities'] = $utilityRepository->getAllUtilies();
        return view('admin.post.create', $data);
    }

    public function store(PostRequest $request)
    {
        $this->postRepository->updateOrCreatePost($id = null, $request);
        $request->session()->flash('message', 'Thêm mới thành công');
        return redirect()->route('admin.post.index');
    }

    public function edit($id)
    {
        $post = Post::with('category')->findOrFail($id);
        $utilities = $post->utilities()->pluck('name');
        $commue = $post->commune()->with('district')->get();
        return response()->json([
            'post' => $post,
            'commune' => $commue,
            'utilities' => $utilities
        ], 200);
    }

    public function search(Request $request)
    {
        return $this->postRepository->searchPost($request->search);
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $user = \Auth::user();
        if ($user->can('delete', $post)) {
            $post->utilities()->detach();
            $post->delete();
            return response()->json([
                'messages' => 'Success',
            ], 200);
        } else {
            return response()->json([
                'messages' => 'Không có quyền xóa!'
            ]);
        }
    }

}
