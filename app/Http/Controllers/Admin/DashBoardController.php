<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\AdminRepository;
use App\Repository\PostRepository;
use App\Repository\CategoryRepository;
use App\Repository\UtilityRepository;

class DashBoardController extends Controller
{
    private $adminRepository;
    private $postRepository;
    private $categoryRepository;
    private $utilityRepository;

    function __construct(AdminRepository $adminRepository, PostRepository $postRepository, CategoryRepository $categoryRepository,UtilityRepository $utilityRepository)
    {
        $this->adminRepository = $adminRepository;
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        $this->utilityRepository = $utilityRepository;
    }

    public function index()
    {
        $data['admins'] = $this->adminRepository->getAllAdmin();
        $data['posts'] = $this->postRepository->getAllPost();
        $data['categories'] = $this->categoryRepository->getAllCategory();
        $data['utilities'] =  $this->utilityRepository->getAllUtilies();
        $data['postsOfUser'] = $this->postRepository->groupByPostOfUser();
        return view('admin.dashboard', $data);
    }
}
