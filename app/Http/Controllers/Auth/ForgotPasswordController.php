<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    public function showLinkRequestForm()
    {
        return view('admin.admin.reset-password');
    }
    public function sendResetLinkEmail(Request $request)
    {
        $email = $request->email;
        $user = User::where('email',$email)->first();
        if($user)
        {
            $code_passoword = generate_code();
            return redirect()->back()->with('messages','Mật khẩu mới: '.$code_passoword);
        }
        return redirect()->back()->with('messages','Email không tồn tại');
    }
}
