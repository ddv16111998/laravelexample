<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\CommuneRepository;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function getCommuneByDistrict(Request $request, CommuneRepository $commune)
    {
        if ($request->ajax()) {
            $district_id = $request->district_id;
            $string = '<option selected> -- Chọn xã --</option>';
            foreach ($commune->getCommunesByDistrict($district_id) as $value) {
                $string .= '<option value="' . $value->id . '">' . $value->name . '</option>';
            }
            return $string;
        }
    }
}
