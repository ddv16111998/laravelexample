<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class EditAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->id;
        return [
            'name'=>'required',
            'username'=>'required',
            'email'=>'required|unique:users,email,'.$id,
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'username.required'=>'Biệt danh không được để trống',
            'email.required'=>'Email không được để trống',
            'email.unique'=>'Email đã tồn tại!',
        ];
    }
}
