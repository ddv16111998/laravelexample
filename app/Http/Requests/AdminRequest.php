<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Rules\AdminRule;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name'=>'required',
            'username'=>['required',new AdminRule()],
            'email'=>'required|unique:users,email',
            'password'=>'required|min:6|max:32',
            'confirmPassword'=>'same:password'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'username.required'=>'Biệt danh không được để trống',
            'email.required'=>'Email không được để trống',
            'email.unique'=>'Email đã tồn tại!',
            'password.required'=>'Mật khẩu không được để trống',
            'password.min'=>'Mật khẩu phải chứa ít nhất :min kí tự',
            'password.max'=>'Mật khẩu chỉ chứa tối đa :max kí tự',
            'confirmPassword.same'=>'Mật khẩu không trùng khớp'
        ];
    }
}
