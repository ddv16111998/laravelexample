<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'newPassword' => 'required|min:6|max:32',
            'confirmPassword' => 'same:newPassword',
        ];
    }
    public function messages()
    {
        return [
            'newPassword.required' => 'Mật khẩu không được để trống',
            'newPassword.min' => 'Mật khẩu chứa ít nhất :min kí tự',
            'newPassword.max' => 'Mật khẩu chứa nhiều nhất :max kí tự',
            'confirmPassword.same' => 'Mật khẩu không đồng nhất',
        ];
    }
}
