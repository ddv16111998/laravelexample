<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'address' => 'required',
            'price' => 'required',
            'acreage' => 'required',
            'district_id' => 'required',
            'commune_id' => 'required',
            'category_id' => 'required',
            'images' => 'required',
            'phone' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Tiêu đề không được để trống',
            'address.required' => 'Địa chỉ không được để trống',
            'price.required' => 'Giá tiền không được để trống',
            'acreage.required' => 'Diện tích không được để trống',
            'district_id.required' => 'Huyện / phường không được để trống',
            'commune_id.required' => 'Xã không được để trống',
            'category_id.required' => 'Danh mục không được để trống',
            'images.required' => 'Ảnh không được để trống',
            'phone.required' => 'Số điện thoại không được để trống',
        ];
    }
}
