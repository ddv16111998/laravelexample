<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UtilityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->id ? $request->id : "";
        return [
            'name' => 'required|unique:utilities,name,' . $id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên tiện ích không được để trống',
            'name.unique' => 'Tên tiện ích đã tồn tại',
        ];
    }
}
