<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $table = 'communes';
    protected $fillable = ['name', 'slug', 'district_id'];

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }
}
