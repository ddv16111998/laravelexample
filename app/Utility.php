<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utility extends Model
{
    protected $table = 'utilities';
    protected $fillable = ['name'];

    public function post()
    {
        return $this->belongsToMany('App\Post', 'post_utility', 'utility_id');
    }
}
