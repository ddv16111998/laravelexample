<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'title', 'address', 'price', 'acreage', 'district_id', 'commune_id', 'category_id', 'description', 'user_id', 'phone', 'images', 'created_at'
    ];
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    public function commune()
    {
        return $this->belongsTo('App\Commune','commune_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function utilities()
    {
        return $this->belongsToMany('App\Utility','post_utility','post_id');
    }
}
